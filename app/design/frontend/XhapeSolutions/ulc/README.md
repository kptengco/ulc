#####Product > Categories
*Add Root Categories:*
> Merchants

> Exclusive Offers

> Certificates

> Cards

> Products

#####Stores > All Stores
*Add stores under Main Website:*
> Merchant Store (merchant root category)

> Exclusive Offer Store (exclusive offers root category)

> Certificates Store (certificates root category)

> Membership Store (cards root category)

> Distributor Store (cards root category)

> Products Store (products root category)

#####Stores > All Stores
*Add store view for the ff stores:*
> Main Website
>> Merchant Store
>>> Merchant Store View
>>>> merchants (code)

> Main Website
>> Exclusive Offer Store
>>> Exclusive Offer Store View
>>>> offers (code)

> Main Website
>> Certificates Store
>>> Certificates Store View
>>>> certificates (code)

> Main Website
>> Membership Store
>>> Membership Store View
>>>> membership_store_view

> Main Website
>> Distributor Store
>>> Distributor Store View
>>>> distributor_store_view

> Main Website
>> Products
>>> Products View
>>>> products_view

#####Stores > Config > Advanced > Advanced
*Disable the ff modules:*
> Magento_Search

#####Stores > Config > Advanced > Developer

> Template Settings
>> Minify HTML

> JavaScript Settings
>> Merge JavaScript Files: Yes
>> Enable JavaScript Bundling: Yes
>> Minify JavaScript Files: Yes

> CSS Settings
>> Merge CSS Files: Yes
>> Minify CSS Files: Yes

#####Stores > Config > General > General > Stores Information

> Stores Name: The Ultimate Lifestyle Card

> Stores Phone Number: +63 (2) 777-2992

#####Stores > Config > General > Stores Email Addresses

> General Contact    
>> Sender Name: The Ultimate Lifestyle Card
>> Sender Email: enquiry@theultimatelifestylecard.com

#####Content > Blocks > New Block

> Block Title: Footer Company Address
>> Identifier: footer.company.address 

>> Stores View: (leave default value)  

>> Content: (copy address from current website’s footer)

> Block Title: Footer Company Contact 
>> Identifier: footer.company.contact 

>> Stores View: (leave default value) 

>>  Content: (copy contact from current website’s footer)

> Block Title: Merchant Logos 
>> Identifier: ulc_merchant_logos  

>> Stores View: (leave default value)  

>> Content: Add images

> Block Title: Merchant Banner Page
>> Identifier: ulc_merchant_banner 

>> Stores View: (leave default value) 

>> Content: Add images

> Block Title: Membership Registration Agreement
>> Identifier: membership.agreement 

>> Stores View: (leave default value) 

>> Content: Add text

> Block Title: Membership Registration Term
>> Identifier: membership.term 

>> Stores View: (leave default value) 

>> Content: Add text

#####Content > Add/Edit Pages  
*Note: Under Design section select 1 column layout*

> Merchant Dashboard Mobile Page 
>> Content: {{block class="Magento\Framework\View\Element\Template" template="Magento_Theme::html/static/merchant_dashboard.phtml"}}

>> SEO Url: /merchant-dashboard-form

>> Layout: 1 column

> Merchant Dashboard Page 
>> Content: {{block class="Magento\Framework\View\Element\Template" template="Magento_Theme::html/static/merchant_dashboard.phtml"}}

>> SEO Url: /merchant-dashboard

>> Layout: 1 column

> Home Page 
>> Content: {{block class="Magento\Framework\View\Element\Template" template="Magento_Theme::html/static/homepage.phtml"}}

>> SEO Url: /home

>> Layout: 1 column

> About Page
>> Content:  {{block class="Magento\Framework\View\Element\Template" template="Magento_Theme::html/static/aboutpage.phtml"}}

>> SEO Url: /about

>> Layout: 1 column

> Careers Page
>> Content: {{block class="Magento\Framework\View\Element\Template" template="Magento_Theme::html/static/careerpage.phtml"}}

>> SEO Url: /careers)

>> Layout: 1 column

> Privileges Page 
>> Content:  {{block class="Magento\Framework\View\Element\Template" template="Magento_Theme::html/static/priviledgepage.phtml"}}

>> SEO Url: /privileges

>> Layout: 1 column

> Help Page
>> Content: {{block class="Magento\Framework\View\Element\Template" template="Magento_Theme::html/static/helppage.phtml"}}

>> SEO Url: /help

>> Layout: 1 column

> Partner Page 
>> Content: {{block class="Magento\Framework\View\Element\Template" template="Magento_Theme::html/static/partnerpage.phtml"}}

>> SEO Url: /partner

>> Layout: 1 column

> How to Save Money Page 
>> Content:  {{block class="Magento\Framework\View\Element\Template" template="Magento_Theme::html/static/howtopage.phtml"}}

>> SEO Url: /how-to-save-money

>> Layout: 1 column

> Customer Service Page 
>> Content:  {{block class="Magento\Framework\View\Element\Template" template="Magento_Theme::html/static/customerservicepage.phtml"}}

>> SEO Url:  /customer-service

>> Layout: 1 column

> Login Page 
>> Content: {{block class="Magento\Framework\View\Element\Template" template="Magento_Theme::html/static/loginpage.phtml"}}

>> SEO Url: /login

>> Layout: 1 column

> Merchant Page 
>> Content: {{block class="Magento\Framework\View\Element\Template" template="Magento_Theme::html/static/merchantpage.phtml"}}

>> SEO Url: /our-merchants

>> Layout: 1 column

> Welcome Page 
>> Content: {{block class="Magento\Framework\View\Element\Template" template="Magento_Theme::html/static/welcomepage.phtml"}}

>> SEO Url: /welcome

>> Layout: 1 column

> Videos Page 
>> Content: {{block class="Magento\Framework\View\Element\Template" video_highlight="" template="Magento_Theme::html/static/videospage.phtml"}}

>> Note: block -> video_highlight should be the sku

>> SEO Url: /events

>> Layout: 1 column

>> Layout Update XML: <head><script src="js/jquery-2.2.4.min.js"/><script src="vendors/bootstrap-3.3.7/js/bootstrap.min.js"/></head>

>> (Add Attribute in Product, label: Video Highlight, code: video_highlight)

> Become a Member Page 
>> Content: {{block class="Magento\Framework\View\Element\Template" template="Magento_Theme::html/static/become_a_member_page.phtml"}}

>> SEO Url: /become-a-member-page

>> Layout: 1 column

>> Layout Update XML: <head><script src="js/jquery-2.2.4.min.js"/><script src="vendors/bootstrap-3.3.7/js/bootstrap.min.js"/><script src="js/membership-paymaya-checkout.js"/></head>

> Product Page 
>> Content: {{block class="Magento\Framework\View\Element\Template" template="Magento_Theme::html/static/productpage.phtml"}}

>> SEO Url: /products

>> Layout: 1 column

> Become a Member Options
>> Content: {{block class="Magento\Framework\View\Element\Template" template="Magento_Theme::html/static/become_a_member_option_page.phtml"}}

>> SEO Url: /become-a-member

>> Layout: 1 column

> Become a Member
>> Content: {{block class="Magento\Framework\View\Element\Template" template="Magento_Theme::html/forms/ulc_store_payment_multiple_form.phtml"}}

>> SEO Url: /membership

>> Layout: 1 column

>> Layout Update XML: <head><script src="js/jquery-2.2.4.min.js"/><script src="vendors/bootstrap-3.3.7/js/bootstrap.min.js"/><script src="js/membership-paymaya-checkout.js"/></head>

> Become a Distributor Page 
>> Content: {{block class="Magento\Framework\View\Element\Template" template="Magento_Theme::html/static/become_a_distributor_page.phtml"}}

>> SEO Url: /become-a-distributor

>> Layout: 1 column

>> Layout Update XML: <head><script src="js/jquery-2.2.4.min.js"/><script src="vendors/bootstrap-3.3.7/js/bootstrap.min.js"/><script src="js/distributor-paymaya-checkout.js"/></head>

#####Content > Add/Edit Pages  
*Note: Under Design section select empty layout*

> Iframe for Become a Member
>> Content: {{block class="Magento\Framework\View\Element\Template" template="Magento_Theme::html/static/become_a_member_iframe_page.phtml"}}

>> SEO Url: /become-a-member-form

>> Layout: empty layout

>> Layout Update XML: <referenceBlock name="ulc_footer" remove="true"/><head><script src="js/jquery-2.2.4.min.js"/><script src="vendors/bootstrap-3.3.7/js/bootstrap.min.js"/><script src="js/membership-paymaya-checkout.js"/></head>

> Page for transactions
>> Content: {{block class="Magento\Framework\View\Element\Template" template="Magento_Theme::html/static/payment_transaction_status.phtml"}}

>> SEO Url: /product-transaction, /membership-card-transaction

>> Layout: 1 column

> Page for Customer Dashboard
>> Content: {{block class="Magento\Framework\View\Element\Template" template="Magento_Theme::html/static/customer_dashboard.phtml"}}
>> SEO Url: /customer-dashboard

>> Layout: 1 column

#####Content > Config > Design Config
*Update the theme to "ULC" of ff Stores:*
> Default Store View

> Merchant Store View

> Exclusive Offer Store View

#####Stores > Product > Attributes > Add New Attributes
> Label: Contact Info
>> Properties
>>> Type: Textarea

>>> Code: contact_info

>>> Add to Column Options: No

>>> Use in Filter Options: No

>> Storefront Properties
>>> Enable WYSIWYG: Yes

>>> Visible on Catalog: Yes

> Label: Discount Details
>> Properties
>>> Type: Textarea

>>> Code: discount_details

>>> Add to Column Options: No

>>> Use in Filter Options: No

>> Storefront Properties
>>> Enable WYSIWYG: Yes

>>> Visible on Catalog: Yes

> Label: Web Discount Details
>> Properties
>>> Type: Textarea

>>> Code: web_discount_details

>>> Add to Column Options: No

>>> Use in Filter Options: No

>> Storefront Properties
>>> Enable WYSIWYG: No

>>> Visible on Catalog: Yes

> Label: Discount Exclusions
>> Properties
>>> Type: Textarea

>>> Code: discount_exclusions

>>> Add to Column Options: No

>>> Use in Filter Options: No

>> Storefront Properties
>>> Enable WYSIWYG: Yes

>>> Visible on Catalog: Yes

> Label: Short Description
>> Properties
>>> Type: Textarea

>>> Code: short_description

>>> Add to Column Options: No

>>> Use in Filter Options: No

>> Storefront Properties
>>> Enable WYSIWYG: Yes

>>> Visible on Catalog: Yes

> Label: Store Hours
>> Properties
>>> Type: Textarea

>>> Code: store_hours

>>> Add to Column Options: No

>>> Use in Filter Options: No

>> Storefront Properties
>>> Enable WYSIWYG: Yes

>>> Visible on Catalog: Yes

> Label: Videos
>> Properties
>>> Type: Textarea

>>> Code: merchant_videos

>>> Add to Column Options: No

>>> Use in Filter Options: No

>> Storefront Properties
>>> Enable WYSIWYG: Yes

>>> Visible on Catalog: Yes

> Label: Certificates
>> Properties
>>> Type: Textarea

>>> Code: merchant_certificates

>>> Add to Column Options: No

>>> Use in Filter Options: No

>> Storefront Properties
>>> Enable WYSIWYG: Yes

>>> Visible on Catalog: Yes

> Label: Number of Cards
>> Properties
>>> Type: Textfield

>>> Default Value: 1

>>>  Input Validation for Store Owner: Integer Number

>>> Code: number_of_cards

>>> Add to Column Options: No

>>> Use in Filter Options: No

>> Storefront Properties
>>> Enable WYSIWYG: No

>>> Visible on Catalog: Yes

#####For Products
> should use sub-category not the root

> store code url should be enabled

> should be access via store code url e.g. merchant/alchemy-bistro-bar.html

#####For Certificates
> add merchant code for user role (admin)

> add merchant code in merchant product catalog

> add merchant code in certificate product catalog