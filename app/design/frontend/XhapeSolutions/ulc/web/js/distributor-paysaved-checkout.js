(function() {
    'use strict';

    var hints = {
        'input-firstname': {
            content: 'What is your name?',
            label: 'First Name'
        },
        'input-lastname': {
            content: 'What is your name?',
            label: 'Last Name'
        },
        'input-date-year': {
            content: 'What is your birthday? You should be at least 18 years old.',
            label: 'Birthday'
        },
        'input-gender': {
            content: 'What is your gender?',
            label: 'Gender'
        },
        'input-email': {
            content: 'We need your email so we can contact and send you updates',
            label: 'Email Address'
        },
        'input-phone': {
            content: 'We need your phone number so we can contact and to serve you better',
            label: 'Phone Number'
        },
        'input-mobilenumber': {
            content: 'We need your mobile number so we can serve you faster and better',
            label: 'Mobile Number'
        },
        'input-address': {
            content: 'What is your current home address?',
            label: 'Street Address'
        },
        'input-postalzip': {
            content: 'What is your current postal/zip code?',
            label: 'Postal / Zip Code'
        },
        'input-country': {
            content: 'What is your current country?',
            label: 'Country'
        },
        'input-city': {
            content: 'What is your current city?',
            label: 'City'
        }
    };

    $(document).ready(function() {
        var validateFields = [
            $('#input-firstname'),
            $('#input-lastname'),
            $('#input-date-year'),
            $('#input-gender'),
            $('#input-email'),
            $('#input-phone'),
            $('#input-mobilenumber'),
            $('#input-address'),
            $('#input-postalzip'),
            $('#input-country'),
            $('#input-city')
        ];

        $.each(validateFields, function(index, field) {
            field.popover({
                container: 'body',
                content: hints[field.attr('id')].content || null,
                placement: 'top',
                title: hints[field.attr('id')].label || null,
                trigger: 'manual'
            });

            field.on('focus', function() {
                $(this).popover('show');
            });

            field.on('blur', function() {
                $(this).popover('hide');
            });
        });

        $('#btn-submit-paysaved-distributor').on('click', function(e) {
            var validForm = true;
            e.preventDefault();

            $.each(validateFields, function(index, field) {
                if (!$.trim(field.val())) {
                    field.focus();
                    field.popover('show');
                    validForm = false;
                    return validForm;
                }
            });

            var notValidBirthday = function(condition) {
                if (condition) {
                    $('#input-date-year').focus();
                    alert('You should be at least 18 years old to apply a distributor card.');
                }

                return condition;
            };

            var minYearCheck = 18;
            var today = new Date();
            var years = today.getFullYear() - parseInt($('#input-date-year').val());

            if (notValidBirthday(years < minYearCheck)) {
                return;
            }

            if (minYearCheck === years) {
                var currentMonth = (today.getMonth() + 1);
                var selectedMonth = parseInt($('#input-date-month').val());
                var month = currentMonth < selectedMonth;

                if (notValidBirthday(month)) {
                    return;
                }

                if (currentMonth === selectedMonth) {
                    var day = today.getDate() < parseInt($('#input-date-date').val());

                    if (notValidBirthday(day)) {
                        return;
                    }
                }
            }

            if (!$('#accept-agreement').is(':checked') || !$('#accept-term').is(':checked')) {
                alert('You need to accept the terms and conditions before submitting this form');

                return;
            }

            if (validForm) {
                $('#input-birthday').val($('#input-date-year').val() + "-" + $('#input-date-month').val() + "-" + $('#input-date-date').val());

                $.ajax({
                    // How data is sent
                    type: "POST",
                    // Program that will send process charge
                    url: "/distributor/order2/create",
                    // Fields
                    data: {
                        'email': $('#input-email').val(),
                        'firstName': $('#input-firstname').val(),
                        'lastName': $('#input-lastname').val(),
                        'birthday': $('#input-birthday').val(),
                        'gender': $('#input-gender').val(),
                        'address': $('#input-address').val(),
                        'postalCode': $('#input-postalzip').val(),
                        'country': $('#input-country').val(),
                        'phoneNumber': $('#input-phone').val(),
                        'mobileNumber': $('#input-mobilenumber').val(),
                        'city': $('#input-city').val(),
                        'distributorPartnerCode': $('#input_distributor_partner_code').val()
                    },
                    dataType: "json",
                    beforeSend: function() {
                        $('#payment-status .alert-danger').addClass('hidden');
                        $('#payment-status .alert-success').addClass('hidden');
                        $('.modal-action-btn').addClass('hidden');
                        $('#payment-status-modal').modal('show');
                        $('#modal-loader').removeClass('hidden');
                        $('#payment-header-status').text('Processing');
                        $('#btn-submit-paysaved-distributor').attr('disabled', 'disabled');
                    },
                    // What to do next after making the AJAX call
                    // Payment was processed
                    success: function (data) {
                        checkoutPaySaved(data.id, data.amount);
                    },
                    // Something went wrong
                    error: function(e) {
                        $('#payment-header-status').text('Cancelled');
                        $('#payment-status .alert-danger').removeClass('hidden');
                    },
                    complete: function() {
                        $('.modal-action-btn').removeClass('hidden');
                        $('#modal-loader').addClass('hidden');
                        $('#btn-submit-paysaved-distributor').removeAttr('disabled');
                    }
                });
            }

            return false;
        });

        function checkoutPaySaved(orderId, orderPrice) {
            var script = document.createElement("script");
            script.src = "https://paysaved.com/ps/ps.js";
            document.head.appendChild(script);

            script.onload = function() {
                var div = document.createElement("div");
                div.innerHTML = '<div id="psid"></div>';
                document.body.appendChild(div);

                window.ps.loadButton("b0bf8cf6dd5a0d93e03093ee661007c4", orderId, orderPrice);

                var intv = setInterval(function () {
                    var form = document.querySelector("#psid > form");
                    if (form) {
                        clearInterval(intv);
                        form.submit();
                    }
                }, 500);
            };
        }
    });
})();