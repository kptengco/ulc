(function() {
    'use strict';

    var init = function () {
        if (document.getElementById('toggle-nav-menu')) {
            document.getElementById('toggle-nav-menu').onclick = function () {
                var btnStyle = this.querySelector('.glyphicon.glyphicon-menu-hamburger').style.display;

                if (btnStyle == 'block' || btnStyle == '') {
                    this.querySelector('.glyphicon.glyphicon-menu-hamburger').style.display = 'none';
                    this.querySelector('.glyphicon.glyphicon-remove').style.display = 'block';
                    document.getElementById('mobile-nav').style.display = 'block';
                } else {
                    this.querySelector('.glyphicon.glyphicon-menu-hamburger').style.display = 'block';
                    this.querySelector('.glyphicon.glyphicon-remove').style.display = 'none';
                    document.getElementById('mobile-nav').style.display = 'none';
                }
            };
        }
    };

    window.addEventListener("load", init);
})();