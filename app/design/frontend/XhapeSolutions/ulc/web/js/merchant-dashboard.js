(function() {
    'use strict';

    function IdentifyMember() {
        if (document.getElementById("identify-card-membership")) {
            var identify = false;

            document.getElementById("identify-card-membership").onclick = function() {
                if (!identify) {
                    identify = true;

                    var xhr = new XMLHttpRequest();
                    var domError = document.querySelector("#merchant-redeem-message .error");
                    var domSuccess = document.querySelector("#merchant-redeem-message .success");
                    domError.className = domError.className.replace("hidden", "") + " hidden";
                    domError.innerHTML = "";
                    domSuccess.className = domSuccess.className.replace("hidden", "") + " hidden";
                    domSuccess.innerHTML = "";

                    document.getElementById('show-redeem-loader').className = 'show';

                    xhr.onreadystatechange = function() {
                        if (this.readyState === 4) {
                            identify = false;
                            document.getElementById('show-redeem-loader').className = 'hidden';

                            if (this.status === 200) {
                                domSuccess.innerHTML = "<p>" + JSON.parse(xhr.responseText.toString()) + "</p>";
                                domSuccess.className = domSuccess.className.replace("hidden", "");
                                domError.className += " hidden";
                            } else if (this.status === 401) {
                                document.querySelectorAll('[data-open-merchant-login]')[0].click();
                            } else {
                                domError.innerHTML = "<p>Couldn't identify card holder.</p>";
                                domError.className = domError.className.replace("hidden", "");
                            }
                        }
                    };

                    xhr.open("GET", "/merchant/user/identifymember?membership_card=" + document.getElementById('cert-membership-card').value, true);

                    xhr.send();
                }
            }
        }
    }

    var init = function () {
        new IdentifyMember();

        var tab = document.querySelectorAll('#merchant-dashboard-tab li');

        if (tab) {
            for (var index = 0; index <= tab.length; index++) {
                if (typeof tab[index] !== 'undefined') {
                    tab[index].onclick = function() {
                        var li = document.querySelectorAll('#merchant-dashboard-tab li');
                        var content = document.querySelectorAll('.tab-item');
                        var index;

                        for (index = 0; index <= li.length; index++) {
                            if (typeof li[index] !== 'undefined') {
                                li[index].className = '';
                            }
                        }

                        for (index = 0; index <= content.length; index++) {
                            if (typeof content[index] !== 'undefined') {
                                content[index].className = 'tab-item';
                            }
                        }

                        this.className = 'active';
                        var targetContent = this.getAttribute('data-tab');

                        document.getElementById(targetContent).className = 'tab-item active';
                    }
                }
            }
        }

        var initSales = function() {};

        if (document.getElementById('sales-report')) {
            var months = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];

            initSales = function() {
                document.querySelector('#sales-report .table tbody').innerHTML = '';
                document.getElementById('sales-report-loading').className = 'show';

                var xhr = new XMLHttpRequest();

                document.getElementById('sales-report-error').className = "hidden";

                xhr.onreadystatechange = function() {
                    if (this.readyState === 4) {
                        document.getElementById('sales-report-loading').className = 'hidden';

                        if (this.status === 200) {
                            var result = JSON.parse(xhr.responseText.toString());
                            var tbody = [];

                            for (var index in result) {
                                var date = new Date(result[index].dateInvoice);
                                var dateInvoice = [];
                                dateInvoice.push(months[date.getMonth()]);
                                dateInvoice.push(date.getDate());
                                dateInvoice.push(date.getFullYear());

                                var flag = date.getHours() - 12 > 0 ? "PM":"AM";
                                var hour = date.getHours() - 12 > 0 ? date.getHours() - 12 : date.getHours();

                                dateInvoice.push(hour + ":");
                                dateInvoice.push(date.getMinutes() + " " + flag);

                                tbody.push('<tr>');
                                tbody.push('<th>' + result[index].invoiceId + '</th>');
                                tbody.push('<td>' + dateInvoice.join(" ") + '</td>');
                                tbody.push('<td class="text-capitalize">' + result[index].customerLastName + ", " + result[index].customerFirstName + '</td>');
                                tbody.push('<td>' + result[index].productName + '</td>');
                                tbody.push('<td>' + result[index].qty + '</td>');
                                tbody.push('<td class="text-uppercase">' + result[index].orderStatus + '</td>');
                                tbody.push('</tr>');
                            }

                            document.querySelector('#sales-report .table tbody').innerHTML = tbody.join('');
                        } else if (this.status === 401) {
                            document.getElementById('sales-report-error').className = "show";
                            document.querySelectorAll('[data-open-merchant-login]')[0].click();
                        } else {
                            document.getElementById('sales-report-error').className = "show";
                        }
                    }
                };

                xhr.open("GET", "/merchant/user/transactions", true);

                xhr.send();
            };

            document.getElementById('sales-report-retry').onclick = function() {
                initSales();
            };

            initSales();
        }

        var initCertificates = function() {};

        if (document.getElementById('certificates')) {
            initCertificates = function() {
                document.getElementById('certificate-list').innerHTML = '';
                document.getElementById('certificates-loading').className = 'show';

                var xhr = new XMLHttpRequest();

                document.getElementById('certificates-error').className = "hidden";

                xhr.onreadystatechange = function() {
                    if (this.readyState === 4) {
                        document.getElementById('certificates-loading').className = 'hidden';

                        if (this.status === 200) {
                            var result = JSON.parse(xhr.responseText.toString());
                            var div = [];
                            var col = 0;
                            var maxCol = 3;

                            for (var index in result) {
                                if (col === 0) {
                                    div.push('<div class="row">');
                                }

                                div.push('<div class="col-sm-' + maxCol + '">');
                                div.push('<div class="thumbnail">');
                                div.push('<img src="' + result[index].image + '" class="img-responsive" alt=""/>');
                                div.push('<div class="caption">');
                                div.push('<h4>' + result[index].name + '</h4>');
                                div.push('<p><small>Stock Left: ' + result[index].qty + '</small></p>');
                                div.push('<p><small>Code: ' + result[index].code + '</small></p>');
                                div.push('<p class="text-center"><small>');
                                div.push('<button class="btn btn-xs btn-success btn-redeem-certificate" data-code="' + result[index].sku + '" type="button">REDEEM</button>');
                                div.push('<a class="btn btn-xs btn-default" href="' + result[index].url + '" target="' + result[index].sku + '">VIEW CERTIFICATE</a>');
                                div.push('</small></p>');
                                div.push('</div>');
                                div.push('</div>');
                                div.push('</div>');

                                col++;

                                if (col > maxCol) {
                                    div.push('</div>');

                                    col = 0;
                                }
                            }

                            document.getElementById('certificate-list').innerHTML = div.join('');

                            setTimeout(function() {
                                var btnRedeem = document.querySelectorAll('.btn-redeem-certificate');

                                if (btnRedeem) {
                                    for (var index = 0; index < btnRedeem.length; index++) {
                                        if (typeof btnRedeem[index] !== 'undefined') {
                                            btnRedeem[index].onclick = function() {
                                                var certCode = this.getAttribute('data-code');

                                                document.getElementById('cert-sku').value = certCode;

                                                if (document.getElementById('modal-certificate-backdrop').className.indexOf('in') > 0) {
                                                    document.getElementById('modal-certificate-backdrop').className = document.getElementById('modal-certificate-backdrop').className.replace(/in/ig,'');
                                                    document.getElementById('modal-certificate').className = document.getElementById('modal-certificate').className.replace(/in/ig,'');
                                                } else {
                                                    document.getElementById('modal-certificate-backdrop').className += ' in';
                                                    document.getElementById('modal-certificate').className += ' in';
                                                }
                                            }
                                        }
                                    }
                                }
                            });
                        } else {
                            document.getElementById('certificates-error').className = "show";
                        }
                    }
                };

                xhr.open("GET", "/merchant/user/products", true);

                xhr.send();
            };

            document.getElementById('certificates-retry').onclick = function() {
                initCertificates();
            };

            initCertificates();

            document.getElementById('modal-certificate-close').onclick = function() {
                document.getElementById('modal-certificate-backdrop').className = document.getElementById('modal-certificate-backdrop').className.replace(/in/ig,'');
                document.getElementById('modal-certificate').className = document.getElementById('modal-certificate').className.replace(/in/ig,'');
            };

            var redeemLoading = false;

            document.getElementById('btn-checkout-certificate').onclick = function() {
                var self = this;
                var membershipCard = document.getElementById('cert-membership-card').value.replace(/ /ig, '');
                var domError = document.querySelector("#merchant-redeem-message .error");
                var domSuccess = document.querySelector("#merchant-redeem-message .success");
                domError.className = domError.className.replace("hidden", "") + " hidden";
                domError.innerHTML = "";
                domSuccess.className = domSuccess.className.replace("hidden", "") + " hidden";
                domSuccess.innerHTML = "";


                if (!membershipCard) {
                    domError.innerHTML = "<p>Membership card is required</p>";
                    domError.className = domError.className.replace("hidden", "");

                    return;
                }

                if (redeemLoading === true) {
                    return;
                }

                this.disabled = true;

                redeemLoading = true;

                document.getElementById('show-redeem-loader').className = 'show';

                var xhr = new XMLHttpRequest();

                xhr.onreadystatechange = function() {
                    if (this.readyState === 4) {
                        document.getElementById('show-redeem-loader').className = 'hidden';
                        self.disabled = false;
                        redeemLoading = false;

                        var result = JSON.parse(xhr.responseText.toString());

                        if (this.status === 200) {
                            domSuccess.innerHTML = "<p>You have successfully redeem this certificate.</p>";
                            domSuccess.className = domSuccess.className.replace("hidden", "");
                            domError.className += " hidden";

                            document.getElementById('cert-membership-card').value = '';
                            document.getElementById('modal-certificate-close').click();
                            initCertificates();
                            initSales();
                        } else {
                            domSuccess.className = domSuccess.className.replace("hidden", "");
                            domSuccess.className += " hidden";
                            domError.className = domError.className.replace("hidden", "");

                            for (var key in result) {
                                domError.innerHTML += "<p>" + result[key] + "</p>";
                            }
                        }
                    }
                };

                xhr.open("POST", "/merchant/user/redeem", true);

                xhr.setRequestHeader("Content-type", "application/x-www-form-urlencoded");

                xhr.send("sku=" + document.getElementById('cert-sku').value + "&membership_card=" + document.getElementById('cert-membership-card').value);
            };
        }

        if (document.getElementById('merchant-my-account')) {
            var passwordLoading = false;

            document.getElementById('merchant-change-password').onclick = function() {
                var self = this;
                var password = document.getElementById('merchant-new-password').value.replace(/ /ig, '');
                var domError = document.querySelector("#merchant-change-password-message .error");
                var domSuccess = document.querySelector("#merchant-change-password-message .success");
                domError.className = domError.className.replace("hidden", "") + " hidden";
                domError.innerHTML = "";
                domSuccess.className = domSuccess.className.replace("hidden", "") + " hidden";
                domSuccess.innerHTML = "";

                if (!password) {
                    domError.innerHTML = "<p>Password is required</p>";
                    domError.className = domError.className.replace("hidden", "");

                    return;
                }

                if (password != document.getElementById('merchant-confirm-password').value) {
                    domError.innerHTML = "<p>Passwords did not match</p>";
                    domError.className = domError.className.replace("hidden", "");

                    return;
                }

                if (passwordLoading === true) {
                    return;
                }

                this.disabled = true;

                passwordLoading = true;

                document.getElementById('show-changepw-loader').className = 'show';

                var xhr = new XMLHttpRequest();

                xhr.onreadystatechange = function() {
                    if (this.readyState === 4) {
                        document.getElementById('show-changepw-loader').className = 'hidden';
                        self.disabled = false;
                        passwordLoading = false;

                        if (this.status === 200) {
                            document.getElementById('merchant-new-password').value = '';
                            document.getElementById('merchant-confirm-password').value = '';
                            document.getElementById('merchant-current-password').value = '';

                            domSuccess.innerHTML = "<p>You have successfully change your password and you have to re-login.</p>";
                            domSuccess.className = domSuccess.className.replace("hidden", "");
                            domError.className += " hidden";

                            setTimeout(function() {
                                window.location.reload(true);
                            }, 3000);
                        } else {
                            var result = JSON.parse(xhr.responseText.toString());

                            domSuccess.className = domSuccess.className.replace("hidden", "");
                            domSuccess.className += " hidden";
                            domError.className = domError.className.replace("hidden", "");

                            for (var key in result) {
                                domError.innerHTML += "<p>" + result[key] + "</p>";
                            }
                        }
                    }
                };

                xhr.open("POST", "/merchant/user/changepassword", true);

                xhr.setRequestHeader("Content-type", "application/x-www-form-urlencoded");

                xhr.send("new=" + password + "&current=" + document.getElementById('merchant-current-password').value);
            };
        }
    };

    window.addEventListener("load", init);
})();