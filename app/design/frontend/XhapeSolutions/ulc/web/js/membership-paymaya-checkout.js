(function() {
    'use strict';

    var hints = {
        'input-firstname': {
            content: 'What is your name?',
            label: 'First Name'
        },
        'input-lastname': {
            content: 'What is your name?',
            label: 'Last Name'
        },
        'input-date-year': {
            content: 'What is your birthday? You should be at least 18 years old.',
            label: 'Birthday'
        },
        'input-gender': {
            content: 'What is your gender?',
            label: 'Gender'
        },
        'input-email': {
            content: 'We need your email so we can contact and send you updates',
            label: 'Email Address'
        },
        'input-phone': {
            content: 'We need your phone number so we can contact and to serve you better',
            label: 'Phone Number'
        },
        'input-mobilenumber': {
            content: 'We need your mobile number so we can serve you faster and better',
            label: 'Mobile Number'
        },
        'input-address': {
            content: 'What is your current home address?',
            label: 'Street Address'
        },
        'input-postalzip': {
            content: 'What is your current postal/zip code?',
            label: 'Postal / Zip Code'
        },
        'input-country': {
            content: 'What is your current country?',
            label: 'Country'
        },
        'input-city': {
            content: 'What is your current city?',
            label: 'City'
        }
    };

    function MultipleOrder() {
        var groupCount = parseInt($('#group_count').val());
        var validateFields = {};

        for (var int = 0; int < groupCount; int++) {
            if (!validateFields["input-firstname"]) {
                validateFields["input-firstname"] = [];
                validateFields["input-lastname"] = [];
                validateFields["input-date-year"] = [];
                validateFields["input-gender"] = [];
                validateFields["input-email"] = [];
                validateFields["input-phone"] = [];
                validateFields["input-mobilenumber"] = [];
                validateFields["input-address"] = [];
                validateFields["input-postalzip"] = [];
                validateFields["input-country"] = [];
                validateFields["input-city"] = [];
            }

            validateFields["input-firstname"].push($('#input-firstname_'+ int));
            validateFields["input-lastname"].push($('#input-lastname_'+ int));
            validateFields["input-date-year"].push($('#input-date-year_'+ int));
            validateFields["input-gender"].push($('#input-gender_'+ int));
            validateFields["input-email"].push($('#input-email_'+ int));
            validateFields["input-phone"].push($('#input-phone_'+ int));
            validateFields["input-mobilenumber"].push($('#input-mobilenumber_'+ int));
            validateFields["input-address"].push($('#input-address_'+ int));
            validateFields["input-postalzip"].push($('#input-postalzip_'+ int));
            validateFields["input-country"].push($('#input-country_'+ int));
            validateFields["input-city"].push($('#input-city_'+ int));
        }

        $.each(validateFields, function(index, fields) {
            $.each(fields, function(index, field) {
                field.popover({
                    container: 'body',
                    content: hints[field.attr('hint-id')].content || null,
                    placement: 'top',
                    title: hints[field.attr('hint-id')].label || null,
                    trigger: 'manual'
                });

                field.on('focus', function() {
                    $(this).popover('show');
                });

                field.on('blur', function() {
                    $(this).popover('hide');
                });
            });
        });

        $('#btn-submit').on('click', function(e) {
            var validForm = true;
            e.preventDefault();

            $.each(validateFields, function(index, fields) {
                $.each(fields, function(index, field) {
                    if (!$.trim(field.val())) {
                        field.focus();
                        field.popover('show');
                        validForm = false;
                    }
                });
            });

            if (!validForm) {
                return;
            }

            var notValidBirthday = function(condition, dom) {
                if (condition) {
                    dom.focus();
                    alert('You should be at least 18 years old to apply a membership card.');
                }

                return condition;
            };

            $.each(document.querySelectorAll('.input-date-year'), function (index, dom) {
                var minYearCheck = 18;
                var today = new Date();
                var years = today.getFullYear() - parseInt($(dom).val());

                if (notValidBirthday(years < minYearCheck, $(dom))) {
                    validForm = false;
                    return;
                }

                if (minYearCheck === years) {
                    var currentMonth = (today.getMonth() + 1);
                    var selectedMonth = parseInt($('#input-date-month_' + index).val());
                    var month = currentMonth < selectedMonth;

                    if (notValidBirthday(month, $(dom))) {
                        validForm = false;
                        return;
                    }

                    if (currentMonth === selectedMonth) {
                        var day = today.getDate() < parseInt($('#input-date-date_' + index).val());

                        if (notValidBirthday(day, $(dom))) {
                            validForm = false;
                            return;
                        }
                    }
                }

                // valid
                $('#input-birthday_' + index).val(
                    $('#input-date-year_' + index).val() + "-" + $('#input-date-month_' + index).val() + "-" + $('#input-date-date_' + index).val()
                );
            });

            if (!$('#accept-agreement').is(':checked') || !$('#accept-term').is(':checked')) {
                alert('You need to accept the terms and conditions before submitting this form');

                return;
            }

            if (validForm) {
                var multipleMember = [];

                for (var intCount = 0; intCount < groupCount; intCount++) {
                    multipleMember.push({
                        'email': $('#input-email_' + intCount).val(),
                        'firstName': $('#input-firstname_' + intCount).val(),
                        'lastName': $('#input-lastname_' + intCount).val(),
                        'birthday': $('#input-birthday_' + intCount).val(),
                        'gender': $('#input-gender_' + intCount).val(),
                        'address': $('#input-address_' + intCount).val(),
                        'postalCode': $('#input-postalzip_' + intCount).val(),
                        'country': $('#input-country_' + intCount).val(),
                        'phoneNumber': $('#input-phone_' + intCount).val(),
                        'mobileNumber': $('#input-mobilenumber_' + intCount).val(),
                        'city': $('#input-city_' + intCount).val()
                    });
                }

                $.ajax({
                    // How data is sent
                    type: "POST",
                    // Program that will send process charge
                    url: "/membership/multipleorder/create",
                    // Fields
                    data: {
                        'multiple': multipleMember,
                        'partnerCode': $('#partner_code').val(),
                        'card': $('#card').val()
                    },
                    dataType: "json",
                    beforeSend: function() {
                        $('#payment-status .alert-danger').addClass('hidden');
                        $('#payment-status .alert-success').addClass('hidden');
                        $('.modal-action-btn').addClass('hidden');
                        $('#payment-status-modal').modal('show');
                        $('#modal-loader').removeClass('hidden');
                        $('#payment-header-status').text('Processing');
                        $('#btn-submit').attr('disabled', 'disabled');
                    },
                    // What to do next after making the AJAX call
                    // Payment was processed
                    success: function (data) {
                        window.location = data;
                    },
                    // Something went wrong
                    error: function(e) {
                        $('#payment-header-status').text('Cancelled');
                        $('#payment-status .alert-danger').removeClass('hidden');
                    },
                    complete: function() {
                        $('.modal-action-btn').removeClass('hidden');
                        $('#modal-loader').addClass('hidden');
                        $('#btn-submit').removeAttr('disabled');
                    }
                });
            }
        });
    }

    $(document).ready(function() {
        if (!document.getElementById("multiple-form")) {
            var validateFields = [
                $('#input-firstname'),
                $('#input-lastname'),
                $('#input-date-year'),
                $('#input-gender'),
                $('#input-email'),
                $('#input-phone'),
                $('#input-mobilenumber'),
                $('#input-address'),
                $('#input-postalzip'),
                $('#input-country'),
                $('#input-city')
            ];

            $.each(validateFields, function(index, field) {
                field.popover({
                    container: 'body',
                    content: hints[field.attr('id')].content || null,
                    placement: 'top',
                    title: hints[field.attr('id')].label || null,
                    trigger: 'manual'
                });

                field.on('focus', function() {
                    $(this).popover('show');
                });

                field.on('blur', function() {
                    $(this).popover('hide');
                });
            });

            $('#btn-submit').on('click', function(e) {
                var validForm = true;
                e.preventDefault();

                $.each(validateFields, function(index, field) {
                    if (!$.trim(field.val())) {
                        field.focus();
                        field.popover('show');
                        validForm = false;
                        return validForm;
                    }
                });

                var notValidBirthday = function(condition) {
                    if (condition) {
                        $('#input-date-year').focus();
                        alert('You should be at least 18 years old to apply a membership card.');
                    }

                    return condition;
                };

                var minYearCheck = 18;
                var today = new Date();
                var years = today.getFullYear() - parseInt($('#input-date-year').val());

                if (notValidBirthday(years < minYearCheck)) {
                    return;
                }

                if (minYearCheck === years) {
                    var currentMonth = (today.getMonth() + 1);
                    var selectedMonth = parseInt($('#input-date-month').val());
                    var month = currentMonth < selectedMonth;

                    if (notValidBirthday(month)) {
                        return;
                    }

                    if (currentMonth === selectedMonth) {
                        var day = today.getDate() < parseInt($('#input-date-date').val());

                        if (notValidBirthday(day)) {
                            return;
                        }
                    }
                }

                if (!$('#accept-agreement').is(':checked') || !$('#accept-term').is(':checked')) {
                    alert('You need to accept the terms and conditions before submitting this form');

                    return;
                }

                if (validForm) {
                    $('#input-birthday').val($('#input-date-year').val() + "-" + $('#input-date-month').val() + "-" + $('#input-date-date').val());

                    $.ajax({
                        // How data is sent
                        type: "POST",
                        // Program that will send process charge
                        url: "/membership/order2/create",
                        // Fields
                        data: {
                            'email': $('#input-email').val(),
                            'firstName': $('#input-firstname').val(),
                            'lastName': $('#input-lastname').val(),
                            'birthday': $('#input-birthday').val(),
                            'gender': $('#input-gender').val(),
                            'address': $('#input-address').val(),
                            'postalCode': $('#input-postalzip').val(),
                            'country': $('#input-country').val(),
                            'phoneNumber': $('#input-phone').val(),
                            'mobileNumber': $('#input-mobilenumber').val(),
                            'city': $('#input-city').val(),
                            'partnerCode': $('#partner_code').val()
                        },
                        dataType: "json",
                        beforeSend: function() {
                            $('#payment-status .alert-danger').addClass('hidden');
                            $('#payment-status .alert-success').addClass('hidden');
                            $('.modal-action-btn').addClass('hidden');
                            $('#payment-status-modal').modal('show');
                            $('#modal-loader').removeClass('hidden');
                            $('#payment-header-status').text('Processing');
                            $('#btn-submit').attr('disabled', 'disabled');
                        },
                        // What to do next after making the AJAX call
                        // Payment was processed
                        success: function (data) {
                            window.location = data;
                        },
                        // Something went wrong
                        error: function(e) {
                            $('#payment-header-status').text('Cancelled');
                            $('#payment-status .alert-danger').removeClass('hidden');
                        },
                        complete: function() {
                            $('.modal-action-btn').removeClass('hidden');
                            $('#modal-loader').addClass('hidden');
                            $('#btn-submit').removeAttr('disabled');
                        }
                    });
                }

                return false;
            });
        } else {
            new MultipleOrder();
        }
    });
})();