(function() {
    'use strict';

    var init = function () {
        if (document.querySelectorAll('[data-open-merchant-login]').length) {
            var btnMerchantLogin = document.querySelectorAll('[data-open-merchant-login]');

            btnMerchantLogin.forEach(function(item) {
                item.onclick = function () {
                    if (document.getElementById('modal-login-backdrop').className.indexOf('in') > 0) {
                        document.getElementById('modal-login-backdrop').className = document.getElementById('modal-login-backdrop').className.replace(/in/ig,'');
                        document.getElementById('modal-login').className = document.getElementById('modal-login').className.replace(/in/ig,'');
                    } else {
                        document.getElementById('modal-login-backdrop').className += ' in';
                        document.getElementById('modal-login').className += ' in';
                    }
                };
            });
        }

        document.getElementById('modal-close').onclick = function() {
            document.getElementById('modal-login-backdrop').className = document.getElementById('modal-login-backdrop').className.replace(/in/ig,'');
            document.getElementById('modal-login').className = document.getElementById('modal-login').className.replace(/in/ig,'');
        };

        var loading;

        document.getElementById('btn-merchant-login').onclick = function() {
            var self = this;
            var username = document.getElementById('merchant-username').value.replace(/ /ig, '');
            var password = document.getElementById('merchant-password').value.replace(/ /ig, '');

            if (!username || !password) {
                alert("Username and password are required");

                return;
            }

            if (loading === true) {
                return;
            }

            document.getElementById('show-login-loader').className = 'show';
            this.disabled = true;

            loading = true;

            var xhr = new XMLHttpRequest();

            xhr.onreadystatechange = function() {
                if (this.readyState === 4) {
                    self.disabled = false;
                    document.getElementById('show-login-loader').className = 'hidden';
                    loading = false;

                    if (this.status === 200) {
                        window.location = document.getElementById('merchant-dashboard-redirect').value;
                    } else {
                        alert("Combination of username and password did not match.");
                    }
                }
            };

            xhr.open("POST", "/merchant/user/login", true);

            xhr.setRequestHeader("Content-type", "application/x-www-form-urlencoded");

            xhr.send("username=" + document.getElementById('merchant-username').value + "&password=" + document.getElementById('merchant-password').value);
        };

        if (document.getElementById('btn-merchant-logout')) {
            document.getElementById('btn-merchant-logout').onclick = function () {
                var xhr = new XMLHttpRequest();

                xhr.onreadystatechange = function() {
                    if (this.readyState === 4 && this.status === 200) {
                        window.location = "http://www.theultimatelifestylecard.com";
                    }
                };

                xhr.open("GET", "/merchant/user/logout", true);

                xhr.send();
            };
        }

        if (window.location.hash === "#merchant-login") {
            document.querySelectorAll('data-open-merchant-login')[0].click();
        }

        if (document.getElementById("toggle-see-password")) {
            document.getElementById("toggle-see-password").onclick = function() {
                var result = this.getAttribute("is-view");

                if (!result) {
                    document.getElementById("merchant-password").type = "text";
                    this.setAttribute("is-view", "1");
                } else {
                    document.getElementById("merchant-password").type = "password";
                    this.setAttribute("is-view", "");
                }
            };
        }
    };

    window.addEventListener("load", init);
})();