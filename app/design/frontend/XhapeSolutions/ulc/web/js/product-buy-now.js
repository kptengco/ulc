(function() {
    var init = function() {

        var buyNowFilters = document.body.innerHTML.match(/(\[.*\|.*\|btn_buy_now\])/g);

        if (buyNowFilters && buyNowFilters.length) {
            for (var key in buyNowFilters) {
                var attrs = buyNowFilters[key].split("|");
                var btn = document.createElement("button");
                btn.type = "button";
                btn.innerText = attrs[0].replace("[", "");
                btn.className = "btn btn-success";
                btn.setAttribute("data-product-buy-now", attrs[1]);

                document.body.innerHTML = document.body.innerHTML
                    .replace(buyNowFilters[key], btn.outerHTML);
            }
        }

        if (document.querySelectorAll('[data-product-buy-now]')) {
            var buttons = document.querySelectorAll('[data-product-buy-now]');

            var buyNow = function() {
                var self = this;
                var sku = this.getAttribute("data-product-buy-now");
                var loading = this.getAttribute("is-loading");

                if (sku && !loading) {
                    document.getElementById('page-loader').className = 'show';
                    this.setAttribute("is-loading", "1");

                    var xhr = new XMLHttpRequest();

                    xhr.onreadystatechange = function() {
                        if (this.readyState === 4) {
                            document.getElementById('page-loader').className = 'hidden';

                            self.setAttribute("is-loading", "");

                            var result = JSON.parse(xhr.responseText.toString());

                            if (this.status === 200) {
                                window.location = result;
                            } else {
                                alert(result.join("\n"));
                            }
                        }
                    };

                    xhr.open("POST", "/product/order2/create", true);

                    xhr.setRequestHeader("Content-type", "application/x-www-form-urlencoded");

                    xhr.send("sku=" + sku);
                }
            };

            for (var key in buttons) {
                buttons[key].onclick = buyNow;
            }
        }
    };

    window.addEventListener("load", function() {
        setTimeout(init, 0);
    });
})();