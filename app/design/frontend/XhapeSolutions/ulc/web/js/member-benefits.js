(function() {
    'use strict';

    var init = function () {
        if (document.getElementById('merchant-certificates-page')) {
            var btnViewCertCode = document.querySelectorAll('.btn-view-cert-code');

            if (btnViewCertCode) {
                for (var index = 0; index < btnViewCertCode.length; index++) {
                    if (typeof btnViewCertCode[index] !== 'undefined') {
                        btnViewCertCode[index].onclick = function() {
                            if (document.getElementById('modal-view-certificate-backdrop').className.indexOf('in') > 0) {
                                document.getElementById('modal-view-certificate-backdrop').className = document.getElementById('modal-view-certificate-backdrop').className.replace(/in/ig,'');
                                document.getElementById('modal-view-certificate').className = document.getElementById('modal-view-certificate').className.replace(/in/ig,'');
                            } else {
                                document.getElementById('modal-view-certificate-backdrop').className += ' in';
                                document.getElementById('modal-view-certificate').className += ' in';
                            }
                        }
                    }
                }

                document.getElementById('modal-view-certificate-close').onclick = function() {
                    document.getElementById('modal-view-certificate-backdrop').className = document.getElementById('modal-view-certificate-backdrop').className.replace(/in/ig,'');
                    document.getElementById('modal-view-certificate').className = document.getElementById('modal-view-certificate').className.replace(/in/ig,'');
                };
            }

            var redeemLoading = false;

            document.getElementById('btn-view-certificate').onclick = function() {
                var self = this;
                var membershipCard = document.getElementById('view-cert-membership-card').value.replace(/ /ig, '');

                if (!membershipCard) {
                    alert("Membership card is required");

                    return;
                }

                if (redeemLoading === true) {
                    return;
                }

                this.disabled = true;

                redeemLoading = true;

                document.getElementById('show-view-certificate-loader').className = 'show';

                var xhr = new XMLHttpRequest();

                xhr.onreadystatechange = function() {
                    if (this.readyState === 4) {
                        document.getElementById('show-view-certificate-loader').className = 'hidden';
                        self.disabled = false;
                        redeemLoading = false;

                        if (this.status === 200) {
                            window.location = '/merchant-certificates?r=1';
                        } else {
                            var result = JSON.parse(xhr.responseText.toString());

                            alert(result.join("\n"));
                        }
                    }
                };

                xhr.open("POST", "/merchant/user/unlockcode", true);

                xhr.setRequestHeader("Content-type", "application/x-www-form-urlencoded");

                xhr.send("month=" + document.getElementById('view-cert-membership-card-expiry-month').value + "&year=" + document.getElementById('view-cert-membership-card-expiry-year').value + "&membership_card=" + document.getElementById('view-cert-membership-card').value);
            };
        }
    };

    window.addEventListener("load", init);
})();