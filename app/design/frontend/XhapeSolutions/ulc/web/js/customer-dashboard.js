(function () {
    'use strict';

    var loading = false;

    function LogoutClass() {
        var logout = function () {
            if (loading === true) {
                return;
            }

            document.getElementById('page-loader').className = 'show';

            loading = true;

            var xhr = new XMLHttpRequest();

            xhr.onreadystatechange = function () {
                if (this.readyState === 4) {
                    document.getElementById('page-loader').className = 'hidden';

                    loading = false;

                    window.location = "/customer/account/login";
                }
            };

            xhr.open("GET", "/member/auth/logout", true);

            xhr.setRequestHeader("Content-type", "application/x-www-form-urlencoded");

            xhr.send();
        };

        if (document.getElementById("btn-mobile-merchant-logout") &&
            document.getElementById("btn-mobile-merchant-logout").onclick) {
            document.getElementById('btn-mobile-merchant-logout').onclick = logout;
        }

        if (document.querySelectorAll("[data-btn-customer-logout]")) {
            document.querySelectorAll("[data-btn-customer-logout]").forEach(function (item) {
                item.onclick = logout;
            });
        }
    }

    function CustomerChangePassword() {
        this.openModal = function () {
            document.getElementById('modal-customer-change-password-backdrop').className += " in";
            document.getElementById('modal-customer-change-password').className += " in";
        };

        // close modal
        document.getElementById('modal-customer-change-password-close').onclick = function () {
            document.getElementById('modal-customer-change-password-backdrop').className = document.getElementById('modal-customer-change-password-backdrop').className.replace(/in/ig, '');
            document.getElementById('modal-customer-change-password').className = document.getElementById('modal-customer-change-password').className.replace(/in/ig, '');
        };

        var changePassword = function () {
            if (document.getElementById('customer-new-password').value.length < 8) {
                var msg = [
                    "Password should be at least 8 characters",
                    "including 3 letters with at least 1 capital letter",
                    "and has number(s)"
                ];

                alert(msg.join("\n"));
                return;
            }

            if (
                !document.getElementById('customer-current-password').value ||
                !document.getElementById('customer-new-password').value ||
                !document.getElementById('customer-confirm-password').value
            ) {
                alert("Current password, new password and confirm password fields are required.");
                return;
            }

            if (
                document.getElementById('customer-new-password').value !==
                document.getElementById('customer-confirm-password').value
            ) {
                alert("Password and confirm password fields does not match.");
                return;
            }

            if (loading === true) {
                return;
            }

            var self = this;

            document.getElementById('show-change-password-loader').className = 'show';

            loading = true;
            this.disabled = true;

            var xhr = new XMLHttpRequest();

            xhr.onreadystatechange = function () {
                if (this.readyState === 4) {
                    self.disabled = false;

                    document.getElementById('show-change-password-loader').className = 'hidden';

                    loading = false;

                    if (this.status === 200) {
                        document.getElementById('customer-current-password').value = "";
                        document.getElementById('customer-new-password').value = "";
                        document.getElementById('customer-confirm-password').value = "";

                        document.getElementById('modal-customer-change-password-close').click();

                        alert("Password successfully updated.");
                    } else {
                        var result = JSON.parse(xhr.responseText.toString());

                        alert(result.join("\n"));
                    }
                }
            };

            xhr.open("POST", "/member/auth/changepassword", true);

            xhr.setRequestHeader("Content-type", "application/x-www-form-urlencoded");

            var postData = "current_password=" + document.getElementById('customer-current-password').value;
            postData += "&password=" + document.getElementById('customer-new-password').value;
            postData += "&confirm_password=" + document.getElementById('customer-confirm-password').value;

            xhr.send(postData);
        };

        if (document.getElementById("open-mobile-change-password")) {
            document.getElementById('open-mobile-change-password').onclick = this.openModal;
        }

        if (document.getElementById("open-change-password")) {
            document.getElementById('open-change-password').onclick = this.openModal;
        }

        document.getElementById("btn-customer-change-password").onclick = changePassword;
    }

    function OrderHistory() {
        var initReport = function () {
        };

        if (document.getElementById('customer-report')) {
            var months = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];

            initReport = function () {
                document.querySelector('#customer-report .table tbody').innerHTML = '';
                document.getElementById('customer-report-loading').className = 'show';

                var xhr = new XMLHttpRequest();

                document.getElementById('customer-report-error').className = "hidden";

                xhr.onreadystatechange = function () {
                    if (this.readyState === 4) {
                        document.getElementById('customer-report-loading').className = 'hidden';

                        if (this.status === 200) {
                            var result = JSON.parse(xhr.responseText.toString());
                            var tbody = [];

                            for (var index in result) {
                                var date = new Date(result[index].dateInvoice);
                                var dateInvoice = [];
                                dateInvoice.push(months[date.getMonth()]);
                                dateInvoice.push(date.getDate());
                                dateInvoice.push(date.getFullYear());

                                var flag = date.getHours() - 12 > 0 ? "PM" : "AM";
                                var hour = date.getHours() - 12 > 0 ? date.getHours() - 12 : date.getHours();

                                dateInvoice.push(hour + ":");
                                dateInvoice.push(date.getMinutes() + " " + flag);

                                tbody.push('<tr>');
                                tbody.push('<th>' + result[index].invoiceId + '</th>');
                                tbody.push('<td>' + dateInvoice.join(" ") + '</td>');
                                tbody.push('<td class="text-capitalize">' + result[index].customerLastName + ", " + result[index].customerFirstName + '</td>');
                                tbody.push('<td>' + result[index].productName + '</td>');
                                tbody.push('<td>' + result[index].qty + '</td>');
                                tbody.push('<td class="text-uppercase">' + result[index].orderStatus + '</td>');
                                tbody.push('</tr>');
                            }

                            document.querySelector('#customer-report .table tbody').innerHTML = tbody.join('');
                        } else if (this.status === 401) {
                            alert("Your session has been expired. Please re-login.");

                            document.getElementById('customer-report-error').className = "show";
                            window.location = "/customer/account/login";
                        } else {
                            document.getElementById('customer-report-error').className = "show";
                        }
                    }
                };

                xhr.open("GET", "/member/order/history", true);

                xhr.send();
            };

            document.getElementById('customer-report-retry').onclick = function () {
                initReport();
            };

            initReport();
        }
    }

    function CertificateSeeCodeButtonWidget() {
        var certificateFilters = document.body.innerHTML.match(/(\[.*\|.*\|btn_certificate_see_code\])/g);

        if (certificateFilters && certificateFilters.length) {
            for (var key in certificateFilters) {
                var attrs = certificateFilters[key].split("|");
                var btn = document.createElement("button");
                btn.type = "button";
                btn.innerText = attrs[0].replace("[", "");
                btn.className = "btn btn-success";
                btn.setAttribute("data-certificate-see-code", attrs[1]);

                document.body.innerHTML = document.body.innerHTML
                    .replace(certificateFilters[key], btn.outerHTML);
            }

            document.querySelectorAll("[data-certificate-see-code]")
                .forEach(function (button) {
                    button.onclick = function () {
                        var self = this;
                        var loading = self.getAttribute("is-loading");
                        var sku = this.getAttribute("data-certificate-see-code");

                        if (!loading) {
                            document.getElementById('page-loader').className = 'show';
                            self.setAttribute("is-loading", "1");

                            var xhr = new XMLHttpRequest();

                            xhr.onreadystatechange = function () {
                                if (this.readyState === 4) {
                                    document.getElementById('page-loader').className = 'hidden';
                                    self.setAttribute("is-loading", "");

                                    if (this.status === 200) {
                                        self.setAttribute("is-loading", "1");
                                        var secretCode = JSON.parse(xhr.responseText.toString());

                                        var domExpiryContainer = document.createElement("span");
                                        domExpiryContainer.id = secretCode + "_expiry";

                                        var domCodeContainer = document.createElement("p");
                                        domCodeContainer.id = secretCode;
                                        domCodeContainer.innerText = secretCode + " - code will be hidden in ";
                                        domCodeContainer.appendChild(domExpiryContainer);

                                        var secs = 20000;
                                        var intervalSecs = 1000;

                                        var intervalInstance = setInterval(function() {
                                            if (secs <= 0) {
                                                clearInterval(intervalInstance);
                                                domCodeContainer.remove();
                                                self.setAttribute("is-loading", "");
                                            }

                                            document.getElementById(domExpiryContainer.id).innerText = (secs / 1000).toString() + " sec(s)";
                                            secs -= intervalSecs;
                                        }, intervalSecs);


                                        self.parentNode.insertBefore(domCodeContainer, self.nextSibling);
                                    } else if (this.status === 401) {
                                        if (window.localStorage) {
                                            window.localStorage.setItem("redirect", "/certificates/" + sku + ".html");
                                        }

                                        window.location = "/customer/account/login";
                                    } else {
                                        alert("Failed to process your request. Please try again.");
                                    }
                                }
                            };

                            xhr.open("GET", "/member/certificate/seecode", true);

                            xhr.setRequestHeader("Content-type", "application/x-www-form-urlencoded");

                            xhr.send({
                                sku: sku
                            });
                        }
                    }
                });
        }
    }

    var init = function () {
        new CertificateSeeCodeButtonWidget();

        new LogoutClass();
        var cp = new CustomerChangePassword();

        if (window.location.hash === "#customer-login") {
            cp.openModal();
        }

        new OrderHistory();
    };

    window.addEventListener("load", init);
})();