(function() {

    var init = function () {
        var domSliders = document.querySelectorAll('[carousel-caption]');

        if (!domSliders.length) {
            return false;
        }

        for (var key in domSliders) {
            function CarouselCaption() {
                var sliderInterval = parseInt(domSliders[key].getAttribute('carousel-caption') || '7');

                var domImgs = domSliders[key].querySelectorAll('img');

                if (domImgs) {
                    var countSliderImgs = 0;
                    var carouselInner = domSliders[key].querySelector('.carousel-inner');
                    var carouselContent;

                    for(var index in domImgs) {
                        if (!domImgs[index].nodeType) {
                            continue;
                        }

                        carouselContent = document.createElement('div');
                        carouselContent.className = 'item';
                        carouselContent.style.maxHeight = '400px';
                        carouselContent.style.minHeight = '400px';
                        carouselContent.style.overflow = 'hidden';


                        if (countSliderImgs === 0) {
                            carouselContent.className += ' active';
                        }

                        countSliderImgs++;

                        var bg = document.createElement('div');
                        bg.className = "item-bg";
                        bg.style.backgroundImage = 'url(' + domImgs[index].getAttribute('src') + ')';
                        bg.style.backgroundSize = 'cover';
                        bg.style.backgroundPosition = '50%';
                        bg.style.backgroundRepeat = 'no-repeat';
                        bg.style.maxHeight = 'inherit';
                        bg.style.minHeight = 'inherit';
                        bg.style.minWidth = '100%';

                        var caption = document.createElement('div');
                        caption.className = 'carousel-caption';

                        var opacity = false;

                        if (domImgs[index].getAttribute('title')) {
                            opacity = true;

                            var title = document.createElement('h1');
                            title.textContent = domImgs[index].getAttribute('title');
                            caption.appendChild(title);
                        }

                        if (domImgs[index].getAttribute('alt')) {
                            opacity = true;

                            var description = document.createElement('p');
                            description.textContent = domImgs[index].getAttribute('alt');
                            caption.appendChild(description);
                        }

                        if (domImgs[index].getAttribute('longdesc')) {
                            opacity = true;

                            var btnLink = document.createElement('a');
                            btnLink.className = 'btn btn-default';
                            btnLink.href = domImgs[index].getAttribute('longdesc');
                            btnLink.textContent = 'Click Here';
                            caption.appendChild(btnLink);
                        }

                        if (opacity) {
                            carouselContent.style.backgroundColor = 'rgba(0,0,0,.9)';
                            bg.style.filter = 'alpha(opacity=75)';
                            bg.style.opacity = '.75';
                        }

                        carouselContent.appendChild(bg);
                        carouselContent.appendChild(caption);
                        carouselInner.appendChild(carouselContent);

                        domImgs[index].remove();
                    }

                    var prevSlide = function() {
                        var target = carouselInner.querySelector('.active');

                        target.className = 'item active left';

                        var prevTarget = target.previousElementSibling || target.previousSibling;

                        if (!prevTarget) {
                            prevTarget = carouselInner.querySelector('.item');
                        }

                        prevTarget.className = 'item next left';

                        setTimeout(function() {
                            target.className = 'item';
                            prevTarget.className = 'item active';
                        }, 1000);
                    };

                    var nextSlide = function() {
                        var target = carouselInner.querySelector('.active');

                        target.className = 'item active left';

                        var nextTarget = target.nextElementSibling || target.nextSibling;

                        if (!nextTarget) {
                            nextTarget = carouselInner.querySelector('.item');
                        }

                        nextTarget.className = 'item next left';

                        setTimeout(function() {
                            target.className = 'item';
                            nextTarget.className = 'item active';
                        }, 1000);
                    };

                    var slide = function () {
                        return setInterval(function() {
                            if (domImgs.length > 1) {
                                nextSlide();
                            }
                        }, sliderInterval * 1000);
                    };

                    var sliderInstance = slide();

                    domSliders[key].querySelector('.left.carousel-control').onclick = function() {
                        clearInterval(sliderInstance);

                        prevSlide();

                        sliderInstance = slide();
                    };

                    domSliders[key].querySelector('.right.carousel-control').onclick = function() {
                        clearInterval(sliderInstance);

                        nextSlide();

                        sliderInstance = slide();
                    };

                    domSliders[key].className += ' ready';
                }
            }

            new CarouselCaption();
        }
    };

    window.addEventListener("load", init);
})();