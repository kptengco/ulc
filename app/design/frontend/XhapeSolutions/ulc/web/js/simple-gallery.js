(function() {
    var init = function () {
        var domGallery = document.getElementById('simple-gallery');

        if (!domGallery) {
            return false;
        }

        var domImages = domGallery.querySelectorAll('img');

        var domImgs = domGallery.cloneNode(true).querySelectorAll('img');

        var overlay = document.createElement('div');
        overlay.className = 'simple-gallery-overlay hidden';

        var closeBtnOverlay = document.createElement('button');
        closeBtnOverlay.className = 'overlay-close-btn';
        closeBtnOverlay.innerText = 'X';

        closeBtnOverlay.onclick = function() {
            overlay.className += ' hidden';
        };

        overlay.appendChild(closeBtnOverlay);

        var placeholder = document.createElement('div');

        if (domImgs) {
            var page = 1;
            var limit = 1;
            var totalCount = 1;
            var count = 1;
            var domPartition = {};
            var button;

            domImages.forEach(function(el, index) {
                el.setAttribute('index', index);

                (function(img) {
                    img.addEventListener("click", function() {
                        var setPage = parseInt(img.getAttribute('index'), 10) + 1;

                        overlay.className = 'simple-gallery-overlay';
                        page = setPage;
                        refreshGallery();
                    });
                })(el);
            });

            var refreshGallery = function() {
                var currentPartition = domPartition[page] || false;

                if (currentPartition) {
                    domGalleryContainer.innerHTML = '';

                    for (var index in currentPartition) {
                        var domImgContainer = document.createElement('div');

                        domImgContainer.appendChild(currentPartition[index]);

                        domGalleryContainer.appendChild(domImgContainer);
                    }
                }
            };

            var prevPage = document.createElement('div');
            prevPage.className = 'col-xs-1 left-action';

            button = document.createElement('button');
            button.setAttribute('type', 'button');
            button.className = 'btn-link btn-gallery-prev';
            button.innerHTML = '<span class="glyphicon glyphicon-chevron-left"></span>';

            button.onclick = function() {
                if (page === 1) {
                    page = Math.ceil(totalCount / limit);
                } else {
                    page--;
                }

                refreshGallery();
            };

            prevPage.appendChild(button);

            placeholder.appendChild(prevPage);

            var domGalleryContainer = document.createElement('div');
            domGalleryContainer.className = 'col-xs-10 gallery-container clearfix';

            for(var index in domImgs) {
                if (!domImgs[index].nodeType) {
                    continue;
                }

                if (count > limit) {
                    count = 1;
                    page++;
                }

                if (!domPartition[page]) {
                    domPartition[page] = [];
                }

                var domImgContainer = document.createElement('div');
                domImgContainer.className = 'col-sm-12 gallery-inner-container';

                domImgs[index].className = 'img-responsive';

                domImgContainer.appendChild(domImgs[index]);

                domPartition[page].push(domImgContainer);

                if (page === 1) {
                    domGalleryContainer.appendChild(domImgContainer);
                }

                totalCount++;
                count++;
            }

            placeholder.appendChild(domGalleryContainer);

            var nextPage = document.createElement('div');
            nextPage.className = 'col-xs-1 right-action';

            button = document.createElement('button');
            button.setAttribute('type', 'button');
            button.className = 'btn-link btn-gallery-next';
            button.innerHTML = '<span class="glyphicon glyphicon-chevron-right"></span>';

            button.onclick = function() {
                if (Math.ceil(totalCount / limit) === page) {
                    page = 1;
                } else {
                    page++;
                }

                refreshGallery();
            };

            nextPage.appendChild(button);

            placeholder.appendChild(nextPage);

            placeholder.className += ' ready simple-gallery-content';

            if (page === 1) {
                placeholder.className += ' no-paging';
            }

            overlay.appendChild(placeholder);

            domGallery.parentNode.appendChild(overlay);

            page = 1;
        }
    };

    window.addEventListener("load", function () {
        setTimeout(init, 500);
    });
})();