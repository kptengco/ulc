(function() {

    var init = function () {
        var domSlider = document.querySelector('[simple-slider]');

        if (!domSlider) {
            return false;
        }

        var sliderInterval = parseInt(domSlider.getAttribute('simple-slider') || '7');

        var domImgs = domSlider.querySelectorAll('img');

        if (domImgs) {
            var countSliderImgs = 0;
            var carouselInner = domSlider.querySelector('.carousel-inner');
            var carouselContent;

            for(var index in domImgs) {
                if (!domImgs[index].nodeType) {
                    continue;
                }

                carouselContent = document.createElement('div');
                carouselContent.className = 'item';
                carouselContent.style.minHeight = '700px';

                if (countSliderImgs === 0) {
                    carouselContent.className += ' active';
                }

                countSliderImgs++;

                var bg = document.createElement('div');
                bg.style.backgroundImage = 'url(' + domImgs[index].getAttribute('src') + ')';
                bg.style.backgroundSize = 'cover';
                bg.style.backgroundPosition = '50%';
                bg.style.backgroundRepeat = 'no-repeat';
                bg.style.minHeight = '700px';
                bg.style.minWidth = '100%';

                carouselContent.appendChild(bg);
                carouselInner.appendChild(carouselContent);

                domImgs[index].remove();
            }

            var prevSlide = function() {
                var target = carouselInner.querySelector('.active');

                target.className = 'item active left';

                var prevTarget = target.previousElementSibling || target.previousSibling;

                if (!prevTarget) {
                    prevTarget = carouselInner.querySelector('.item');
                }

                prevTarget.className = 'item next left';

                setTimeout(function() {
                    target.className = 'item';
                    prevTarget.className = 'item active';
                }, 1000);
            };

            var nextSlide = function() {
                var target = carouselInner.querySelector('.active');

                target.className = 'item active left';

                var nextTarget = target.nextElementSibling || target.nextSibling;

                if (!nextTarget) {
                    nextTarget = carouselInner.querySelector('.item');
                }

                nextTarget.className = 'item next left';

                setTimeout(function() {
                    target.className = 'item';
                    nextTarget.className = 'item active';
                }, 1000);
            };

            var slide = function () {
                return setInterval(function() {
                    nextSlide();
                }, sliderInterval * 1000);
            };

            var sliderInstance = slide();

            domSlider.querySelector('.left.carousel-control').onclick = function() {
                clearInterval(sliderInstance);

                prevSlide();

                sliderInstance = slide();
            };

            domSlider.querySelector('.right.carousel-control').onclick = function() {
                clearInterval(sliderInstance);

                nextSlide();

                sliderInstance = slide();
            };

            domSlider.className += ' ready';
        }
    };

    window.addEventListener("load", init);
})();