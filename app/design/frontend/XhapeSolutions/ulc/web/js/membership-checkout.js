(function() {
    'use strict';

    $(document).ready(function() {
        var amount = $('#input-membership-price').val(); // get price from magento
        var description = 'Membership Card';

        var validateFields = [
            $('#input-firstname'),
            $('#input-lastname'),
            $('#input-date-year'),
            $('#input-gender'),
            $('#input-email'),
            $('#input-phone'),
            $('#input-mobilenumber'),
            $('#input-address'),
            $('#input-postalzip'),
            $('#input-country'),
            $('#input-city')
        ];

        var hints = {
          'input-firstname': {
              content: 'What is your name?',
              label: 'First Name'
          },
          'input-lastname': {
              content: 'What is your name?',
              label: 'Last Name'
          },
          'input-date-year': {
              content: 'What is your birthday? You should be at least 18 years old.',
              label: 'Birthday'
          },
          'input-gender': {
              content: 'What is your gender?',
              label: 'Gender'
          },
          'input-email': {
              content: 'We need your email so we can contact and send you updates',
              label: 'Email Address'
          },
          'input-phone': {
              content: 'We need your phone number so we can contact and to serve you better',
              label: 'Phone Number'
          },
          'input-mobilenumber': {
              content: 'We need your mobile number so we can serve you faster and better',
              label: 'Mobile Number'
          },
          'input-address': {
              content: 'What is your current home address?',
              label: 'Street Address'
          },
          'input-postalzip': {
              content: 'What is your current postal/zip code?',
              label: 'Postal / Zip Code'
          },
          'input-country': {
              content: 'What is your current country?',
              label: 'Country'
          },
          'input-city': {
              content: 'What is your current city?',
              label: 'City'
          }
        };

        $.each(validateFields, function(index, field) {
            field.popover({
                container: 'body',
                content: hints[field.attr('id')].content || null,
                placement: 'top',
                title: hints[field.attr('id')].label || null,
                trigger: 'manual'
            });

            field.on('focus', function() {
               $(this).popover('show');
            });

            field.on('blur', function() {
                $(this).popover('hide');
            });
        });

        $('#btn-submit').on('click', function(e) {
            var validForm = true;
            e.preventDefault();

            $.each(validateFields, function(index, field) {
                if (!$.trim(field.val())) {
                    field.focus();
                    field.popover('show');
                    validForm = false;
                    return validForm;
                }
            });

            var notValidBirthday = function(condition) {
                if (condition) {
                    $('#input-date-year').focus();
                    alert('You should be at least 18 years old to apply a membership card.');
                }

                return condition;
            };

            var minYearCheck = 18;
            var today = new Date();
            var years = today.getFullYear() - parseInt($('#input-date-year').val());

            if (notValidBirthday(years < minYearCheck)) {
                return;
            }

            if (minYearCheck === years) {
                var currentMonth = (today.getMonth() + 1);
                var selectedMonth = parseInt($('#input-date-month').val());
                var month = currentMonth < selectedMonth;

                if (notValidBirthday(month)) {
                    return;
                }

                if (currentMonth === selectedMonth) {
                    var day = today.getDate() < parseInt($('#input-date-date').val());

                    if (notValidBirthday(day)) {
                        return;
                    }
                }
            }

            if (!$('#accept-agreement').is(':checked') || !$('#accept-term').is(':checked')) {
                alert('You need to accept the terms and conditions before submitting this form');

                return;
            }

            if (validForm) {
                Magpie.open({
                    'name': $('#input-company-name').val(), // get company name from magento
                    'amount': amount,
                    'description': description,
                    'email': $('#input-email').val()
                });
            }

            return false;
        });

        Magpie.configure({
             // The publishable key from the Magpie dashboard
             key: $('#input-merchant-key').val(),   // for new
             // The name of the store -- appears on the checkout page
             name: null, // get company name from magento
             // The description of the product or service being purchased
             description: null,
             // An amount, usually default value, to charge
             amount: 0,
             // The currency
             currency: 'php' ,
             // The URL where Checkout will fetch your logo
             image: $('#input-company-logo').val()
         }, function(err, token) {
             $("#overlay").show();
             // AJAX stuff, check out jQuery AJAX doc

             // ADD THIS
             // Explanation: Checkout passes a different type of payload for first-time remember-me events
             // There is no token.id, so we have to use the card for charging the transaction

             var token_to_pass = token.id;

             if (!token_to_pass) {
                 token_to_pass = token.card.id;
             }

             ////////// END OF CHANGE
            $('#input-birthday').val($('#input-date-month').val() + '/' + $('#input-date-date').val() + '/' + $('#input-date-year').val());

            $.ajax({
                 // How data is sent
                 type: "POST",
                 // Program that will send process charge
                 url: "/membership/order/create",
                 // Fields
                 data: {
                     'token': token_to_pass, // CHANGE HERE
                     'amount': amount,
                     'description': description,
                     'email': token.email,    // ADDITION FOR WHEN YOU WANT TO EMAIL THE DONOR,
                     'firstName': $('#input-firstname').val(),
                     'lastName': $('#input-lastname').val(),
                     'birthday': $('#input-birthday').val(),
                     'gender': $('#input-gender').val(),
                     'address': $('#input-address').val(),
                     'postalCode': $('#input-postalzip').val(),
                     'country': $('#input-country').val(),
                     'phoneNumber': $('#input-phone').val(),
                     'mobileNumber': $('#input-mobilenumber').val(),
                     'city': $('#input-city').val()
                 },
                 dataType: "json",
                 beforeSend: function() {
                     $('#payment-status .alert-danger').addClass('hidden');
                     $('#payment-status .alert-success').addClass('hidden');
                     $('.modal-action-btn').addClass('hidden');
                     $('#payment-status-modal').modal('show');
                     $('#modal-loader').removeClass('hidden');
                     $('#payment-header-status').text('Processing');
                     $('#btn-submit').attr('disabled', 'disabled');
                 },
                 // What to do next after making the AJAX call
                 // Payment was processed
                 success: function (data) {
                     $('#payment-header-status').text('Success');

                     $('#input-firstname').val(null);
                     $('#input-lastname').val(null);
                     $('#input-email').val(null);
                     $('#input-address').val(null);
                     $('#input-postalzip').val(null);
                     $('#input-country').val(null);
                     $('#input-phone').val(null);
                     $('#input-mobilenumber').val(null);
                     $('#input-city').val(null);
                     $('#input-birthday').val(null);

                     $('#payment-status .alert-success').removeClass('hidden');
                     $('#payment-id').text(data);

                     // We load a page if this event is fired.
                     // You can also do other cool notification stuff here
                     // 1. Trigger a modal
                     // 2. Send an email notification.
                     // 3. Send an SMS, if you know the mobile number of the customer.
                     // 4. Etcetera.
                     // window.location.href = "/thanks.html";
                     // window.alert(JSON.stringify(data));
                 },
                 // Something went wrong
                 error: function(e) {
                     $('#payment-header-status').text('Cancelled');
                     $('#payment-status .alert-danger').removeClass('hidden');
                 },
                 complete: function() {
                     $('.modal-action-btn').removeClass('hidden');
                     $('#modal-loader').addClass('hidden');
                     $('#btn-submit').removeAttr('disabled');
                     $("#overlay").hide();
                 }
             });
         });
    });
})();