<?php

namespace XhapeSolutions\ProductOrder\Controller\Order2;

use PayMaya\PayMayaSDK;
use PayMaya\API\Checkout;
use PayMaya\Model\Checkout\Buyer;
use PayMaya\Model\Checkout\ItemAmountDetails;
use PayMaya\Model\Checkout\ItemAmount;
use PayMaya\Model\Checkout\Item;
use PayMaya\Model\Checkout\Contact;
use PayMaya\Model\Checkout\Address;
use PayMaya\API\Webhook;

class Create extends Payment
{
    public function execute()
    {
        try
        {
            parent::execute();

            if ($this->isLogin())
            {
                if (!$this->validateFormRequest())
                {
                    return $this->getJSON();
                }

                $strSku = $this->getEscaper()->escapeHtml($this->getRequest()->getParam('sku'));

                $this->getProduct()->load($this->getProduct()->getIdBySku($strSku));

                $objOrder = $this->checkout();

                $strCheckoutUrl = $this->createOrder($objOrder);

                $this->getJSON()->setData($strCheckoutUrl);
            }
            else
            {
                $this->getJSON()->setHttpResponseCode(\Magento\Framework\Webapi\Exception::HTTP_UNAUTHORIZED);
                $this->getJSON()->setData(["You must be login to continue."]);
            }
        }
        catch(\Exception $objError)
        {
            $this->addLogger([
                $objError->getMessage()
            ]);
        }

        return $this->getJSON();
    }

    private function createOrder($objOrder)
    {
        try
        {
            if (!session_id())
            {
                session_start();
            }

            if (!empty($_POST))
            {
                $strResult = $this->generateCheckoutUrl($objOrder->getEntityId());

                if ($strResult)
                {
                    return $strResult;
                }
                else
                {
                    throw new \Exception("Err: Generating checkout");
                }
            }
            else
            {
                throw new \Exception("Err: No Data");
            }
        }
        catch (\Exception $objError)
        {
            throw new \Exception($objError->getMessage());
        }
    }

    /**
     * @readme: Generate PayMaya checkout
     *
     * @return $strUri
     *
     * */
    private function generateCheckoutUrl($intOrderRef)
    {
        PayMayaSDK::getInstance()->initCheckout(self::$PUBLIC_KEY, self::$API_KEY, self::$PAYMAYA_ENV);

        // Checkout
        $objCheckout = new Checkout();

        // Contact
        $objContact = new Contact();
        $objContact->phone = $this->getCustomerTelephone();
        $objContact->email = $this->getCustomerEmail();

        $arrStreet = $this->getCustomerStreet();
        $strStreet = $arrStreet[0][0];

        // Address
        $objAddress = new Address();
        $objAddress->line1 = $strStreet;
        $objAddress->line2 = "";
        $objAddress->city = $this->getCustomerCity();
        $objAddress->state = "";
        $objAddress->zipCode = $this->getCustomerPostcode();
        $objAddress->countryCode = $this->getCustomerCountryId();

        $objBuyer = new Buyer();
        $objBuyer->firstName = $this->getCustomerFirstname();
        $objBuyer->middleName = null;
        $objBuyer->lastName = $this->getCustomerLastname();
        $objBuyer->contact = $objContact;
        $objBuyer->shippingAddress = $objAddress;
        $objBuyer->billingAddress = $objAddress;

        $objCheckout->buyer = $objBuyer;

        // Item
        $objItemAmountDetails = new ItemAmountDetails();
        $objItemAmountDetails->shippingFee = "0";
        $objItemAmountDetails->tax = "0";
        $objItemAmountDetails->subtotal = $this->getProduct()->getPrice();

        $objItemAmount = new ItemAmount();
        $objItemAmount->currency = "PHP";
        $objItemAmount->value = $this->getProduct()->getPrice();
        $objItemAmount->details = $objItemAmountDetails;

        $strItemDesc = $this->getProduct()->getDescription();
        $strItemDescription = trim($strItemDesc) ? substr(strip_tags($strItemDesc), 1,255) : "no description";

        $objItem = new Item();
        $objItem->name = $this->getProduct()->getName();
        $objItem->code = $this->getProduct()->getSku();
        $objItem->description = $strItemDescription;
        $objItem->quantity = "1";
        $objItem->amount = $objItemAmount;
        $objItem->totalAmount = $objItemAmount;

        $objCheckout->items = array($objItem);
        $objCheckout->totalAmount = $objItemAmount;
        $objCheckout->requestReferenceNumber = (string)$intOrderRef;
        $objCheckout->redirectUrl = array(
            "success" => $this->getStoreBaseUrl() . "product-transaction?status=success&o=" . (str_pad($intOrderRef,10,"0",STR_PAD_LEFT)),
            "failure" => $this->getStoreBaseUrl() . "product-transaction?status=fail",
            "cancel" => $this->getStoreBaseUrl() . "product-transaction?status=cancel"
        );

        $objCheckout->execute();

        $this->registerIPN();

        return $objCheckout->url; // Checkout URL
    }

    private function checkout()
    {
        try
        {
            $objProduct = $this->getProduct();

            if ($objProduct) {
                $this->getCart()->truncate();
                $this->getCart()->addProduct($objProduct, [
                    'qty' => 1
                ]);

                $objQuote = $this->getCart()->getQuote();
                $objQuote->setStore($this->getStoreManager()->getStore("products_view"));

                //Set Customer
                $objQuote->assignCustomer($this->getCustomer());
                $objQuote->setCustomerIsGuest(false);

                $arrStreet = $this->getCustomerStreet();

                if (!isset($arrStreet[0], $arrStreet[0][0]))
                {
                    throw new \Exception("No billing address found");
                }

                $arrAddress = [
                    "region" => "",
                    "region_id" => 0,
                    "country_id" => $this->getCustomerCountryId(),
                    "street" => $arrStreet,
                    "company" => "",
                    "telephone" => $this->getCustomerTelephone(),
                    "postcode" => $this->getCustomerPostcode(),
                    "city" => $this->getCustomerCity(),
                    "firstname" => $this->getCustomerFirstname(),
                    "lastname" => $this->getCustomerLastname(),
                    "email" => $this->getCustomerEmail(),
                    "prefix" => "",
                    "region_code" => ""
                ];

                $objQuote->getBillingAddress()->addData($arrAddress);

                $objQuote
                    ->getShippingAddress()
                    ->collectShippingRates()
                    ->setShippingMethod('freeshipping_freeshipping');

                // Set Sales Order Payment
                $objQuote->getPayment()->setMethod('checkmo');

                $this->getCart()->save();

                // Create Order From Quote
                $objOrder = $this->getQuoteManagement()->submit($objQuote);

                $objOrder->setEmailSent(0);
                $objOrder->setCustomerNoteNotify(false);

                $objOrder->save();

                $fOrder = fopen("product_order_{$objOrder->getEntityId()}.txt", "w");
                fclose($fOrder);

                return $objOrder;
            }
        }
        catch (\Exception $objError)
        {
            /**
             * @todo: for now manually override the fix in mage 2.2 https://github.com/magento/magento2/commit/1fb3a97b4f394ae05d7afc5a213d0eb22d6e7c2e
             *
             * app/code/Magento/Quote/Model/Quote.php
             *
             * public function getItemById($itemId)
             * {
             *      //return $this->getItemsCollection()->getItemById($itemId);
             *      foreach ($this->getItemsCollection() as $item)
             *      {
             *          if ($item->getId() == $itemId)
             *          {
             *              return $item;
             *          }
             *      }
             *      return false;
             * }
             * */
            throw new \Exception($objError->getMessage());
        }

        return false;
    }

    /**
     * @readme: Handle Form Data
     *
     * @return boolean
     *
     **/
    private function validateFormRequest()
    {
        $arrError = [];

        $arrPost = $this->getRequest();
        $objEscaper = $this->getEscaper();

        if (!$objEscaper->escapeHtml($arrPost->getParam('sku')))
        {
            $arrError[] = 'Product is required';
        }

        if (!empty($arrError))
        {
            $this->addLogger($arrError);

            return false;
        }

        return true;
    }

    private function registerIPN()
    {
        if (self::$PAYMAYA_ENV == "SANDBOX")
        {
            $arrHooks = Webhook::retrieve();

            foreach($arrHooks as $objHook)
            {
                $objHook->delete();
            }
        }

        $objWebhook = new Webhook();
        $objWebhook->name = Webhook::CHECKOUT_SUCCESS;
        $objWebhook->callbackUrl = $this->getStoreBaseUrl() . "paymaya/process/paymentsuccess";
        $objWebhook->register();

        // cancelled
        $objWebhook = new Webhook();
        $objWebhook->name = "CHECKOUT_DROPOUT";
        $objWebhook->callbackUrl = $this->getStoreBaseUrl() . "paymaya/process/paymentcancel";
        $objWebhook->register();

        $objWebhook = new Webhook();
        $objWebhook->name = Webhook::CHECKOUT_FAILURE;
        $objWebhook->callbackUrl = $this->getStoreBaseUrl() . "paymaya/process/paymentfail";
        $objWebhook->register();
    }
}