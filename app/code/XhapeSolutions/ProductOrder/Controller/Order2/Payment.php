<?php

namespace XhapeSolutions\ProductOrder\Controller\Order2;

class Payment extends \XhapeSolutions\Paymaya\Controller\Process\Payment
{
    private $objCustomerFactory;
    private $objAddressFactory;
    private $objCustomerRepository;

    private $bolLogin = false;
    private $objCustomer;
    private $objCustomerAddress;

    public function __construct(\Magento\Framework\App\Action\Context $objContext,
                                \Magento\Framework\Controller\Result\JsonFactory $objJsonFactory,
                                \Magento\Catalog\Model\Product $objProduct,
                                \Magento\Checkout\Model\Cart $objCart,
                                \Magento\Quote\Model\QuoteManagement $objQuoteManagement,
                                \Magento\Framework\Escaper $objEscaper,
                                \Magento\Store\Model\StoreManagerInterface $objStoreManager,
                                \Magento\Framework\DB\TransactionFactory $objTransactionFactory,
                                \Magento\Sales\Model\Order $objOrder,
                                \Magento\Customer\Model\CustomerFactory $objCustomerFactory,
                                \Magento\Customer\Api\CustomerRepositoryInterface $objCustomerRepository,
                                \Magento\Customer\Model\AddressFactory $objAddressFactory
    )
    {
        parent::__construct(
            $objContext,
            $objJsonFactory,
            $objProduct,
            $objCart,
            $objQuoteManagement,
            $objEscaper,
            $objStoreManager,
            $objTransactionFactory,
            $objOrder
        );

        $this->objCustomerFactory = $objCustomerFactory;
        $this->objAddressFactory = $objAddressFactory;
        $this->objCustomerRepository = $objCustomerRepository;
    }

    public function execute()
    {
        try
        {
            if (!session_id())
            {
                session_start();
            }

            if (isset($_SESSION["ulc_customer"], $_SESSION["ulc_customer"]["email"]))
            {
                $intWebsiteId = $this->getStoreManager()->getStore()->getWebsiteId();

                $objCustomer = $this->objCustomerFactory->create();

                $objCustomer->setWebsiteId($intWebsiteId);
                $objCustomer->loadByEmail($_SESSION["ulc_customer"]["email"]);

                if ($objCustomer->getId())
                {
                    $this->bolLogin = true;

                    $this->objCustomer = $this->objCustomerRepository->getById($objCustomer->getEntityId());

                    $this->objCustomerAddress = $objCustomer->getDefaultShippingAddress();
                }
            }
        }
        catch (\Exception $objError)
        {
            throw new \Exception(__METHOD__ . ":" . __LINE__ . " - " . $objError->getMessage());
        }
    }

    protected function isLogin()
    {
        return $this->bolLogin;
    }

    protected function getCustomer()
    {
        return $this->objCustomer;
    }

    protected function getCustomerAddress()
    {
        return $this->objCustomerAddress;
    }

    protected function getCustomerFirstname()
    {
        return $this->objCustomerAddress->getFirstname();
    }

    protected function getCustomerLastname()
    {
        return $this->objCustomerAddress->getLastname();
    }

    protected function getCustomerStreet()
    {
        return $this->objCustomerAddress->getStreet();
    }

    protected function getCustomerPostcode()
    {
        return $this->objCustomerAddress->getPostcode();
    }

    protected function getCustomerCity()
    {
        return $this->objCustomerAddress->getCity();
    }

    protected function getCustomerCountryId()
    {
        return $this->objCustomerAddress->getCountryId();
    }

    protected function getCustomerTelephone()
    {
        return $this->objCustomerAddress->getTelephone();
    }

    protected function getCustomerEmail()
    {
        return $_SESSION["ulc_customer"]["email"];
    }
}