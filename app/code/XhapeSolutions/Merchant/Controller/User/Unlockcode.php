<?php

namespace XhapeSolutions\Merchant\Controller\User;

class Unlockcode extends AbstractAction
{
    private $objCustomerCollection;

    public function __construct(
        \Magento\Framework\App\Action\Context $objContext,
        \Magento\Framework\Controller\Result\JsonFactory $objJsonFactory,
        \Magento\Framework\Escaper $objEscaper,
        \Magento\User\Model\User $objUser,
        \Magento\Store\Model\StoreManagerInterface $objStoreManager,
        \Magento\Customer\Model\ResourceModel\Customer\Collection $objCustomerCollection
    )
    {
        parent::__construct($objContext, $objJsonFactory, $objEscaper, $objUser, $objStoreManager);

        $this->objCustomerCollection = $objCustomerCollection;
    }

    protected function afterExecute()
    {
        try
        {
            if (isset($_SESSION['ulc_membership_card_valid']))
            {
                return $this->getJSON();
            }

            $strYearExpiryParam = $this->getEscaper()->escapeHtml($this->getRequest()->getParam('year'));
            $strMonthExpiryParam = $this->getEscaper()->escapeHtml($this->getRequest()->getParam('month'));

            $objCustomerCollection = $this->objCustomerCollection
                ->addAttributeToSelect('*')
                ->addAttributeToFilter('membership_card_number', $this->getEscaper()->escapeHtml($this->getRequest()->getParam('membership_card')))
                ->addAttributeToFilter('membership_card_expiry', ['notnull' => true])
                ->load();

            $arrCustomer = $objCustomerCollection->getData();

            if (!isset($arrCustomer[0]))
            {
                // not found
                $this->getJSON()->setHttpResponseCode(\Magento\Framework\Webapi\Exception::HTTP_BAD_REQUEST);
                $this->getJSON()->setData(["Invalid Membership Card", "Error: IC1"]);
            }
            else if (isset($arrCustomer[0]))
            {
                $strYearExpiry = date('Y', strtotime($arrCustomer[0]['membership_card_expiry']));
                $strMonthExpiry = date('m', strtotime($arrCustomer[0]['membership_card_expiry']));
                $strDateExpiry = date('d', strtotime($arrCustomer[0]['membership_card_expiry']));

                if ($strYearExpiryParam != $strYearExpiry || $strMonthExpiryParam != $strMonthExpiry)
                {
                    $this->getJSON()->setHttpResponseCode(\Magento\Framework\Webapi\Exception::HTTP_BAD_REQUEST);
                    $this->getJSON()->setData(["Invalid Membership Card", "Error: IC2"]);
                }
                else
                {
                    if (date("Y") >= $strYearExpiry &&
                        date("m") >= $strMonthExpiry &&
                        date('d') >= $strDateExpiry
                    )
                    {
                        // expired
                        $this->getJSON()->setHttpResponseCode(\Magento\Framework\Webapi\Exception::HTTP_BAD_REQUEST);
                        $this->getJSON()->setData(["Invalid Membership Card", "Error: IC3"]);
                    }
                    else
                    {
                        $_SESSION['ulc_membership_card_valid'] = 1;
                    }
                }
            }
        }
        catch(Exception $objError)
        {
            $this->getJSON()->setHttpResponseCode(\Magento\Framework\Webapi\Exception::HTTP_INTERNAL_ERROR);
            $this->getJSON()->setData(["Failed to validate membership card number. Please try again."]);
        }

        return $this->getJSON();
    }
}