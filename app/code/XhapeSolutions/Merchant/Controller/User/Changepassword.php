<?php

namespace XhapeSolutions\Merchant\Controller\User;

use Braintree\Exception;

class Changepassword extends AbstractAction
{
    protected $checkAuthorize = true;

    protected function afterExecute()
    {
        try
        {
            $strCurrentPassword = $this->getEscaper()->escapeHtml($this->getRequest()->getParam('current'));
            $strPassword = $this->getEscaper()->escapeHtml($this->getRequest()->getParam('new'));

            if (!$this->getUser()->verifyIdentity($strCurrentPassword))
            {
                $this->getJSON()->setHttpResponseCode(\Magento\Framework\Webapi\Exception::HTTP_BAD_REQUEST);
                $this->getJSON()->setData(["Incorrect current password"]);
            }
            else
            {
                $strRole = strtolower($this->getUser()->getRole()->getData('role_name'));

                if ($strRole !== 'merchant')
                {
                    $this->getJSON()->setHttpResponseCode(\Magento\Framework\Webapi\Exception::HTTP_BAD_REQUEST);
                    $this->getJSON()->setData(["Invalid Account"]);
                }
                else
                {
                    $this->getUser()->setPassword($strPassword);
                    $this->getUser()->save();

                    unset($_SESSION['ulc_admin_sess_user_context_id']);
                }
            }
        }
        catch(\Magento\Framework\Validator\Exception $objError)
        {
            $this->getJSON()->setHttpResponseCode(\Magento\Framework\Webapi\Exception::HTTP_BAD_REQUEST);
            $this->getJSON()->setData([$objError->getMessage()]);
        }
        catch(\Exception $objError)
        {
            $this->getJSON()->setHttpResponseCode(\Magento\Framework\Webapi\Exception::HTTP_BAD_REQUEST);
            $this->getJSON()->setData([$objError->getMessage()]);
        }

        return $this->getJSON();
    }
}