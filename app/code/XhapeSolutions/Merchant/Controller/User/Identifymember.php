<?php

namespace XhapeSolutions\Merchant\Controller\User;

class Identifymember extends Redeem
{
    private function addCustomer()
    {
        $strMembershipCard = $this->getEscaper()->escapeHtml($this->getRequest()->getParam('membership_card'));

        $objCustomerCollection = $this->getCustomerCollection()->addAttributeToFilter('membership_card_number', [
            'eq' => $strMembershipCard
        ])
            ->load()
            ->getData();

        if (isset($objCustomerCollection[0]))
        {
            $this->getCustomer()->load($objCustomerCollection[0]['entity_id']);

            $dtMembershipCardExpiry = $this->getCustomer()->getData('membership_card_expiry');

            if (is_string($dtMembershipCardExpiry))
            {
                return strtotime($dtMembershipCardExpiry);
            }
        }

        return false;
    }

    protected function afterExecute()
    {
        $intTimestampExpiry = $this->addCustomer();

        if ($intTimestampExpiry === false || ($intTimestampExpiry <= time()))
        {
            $this->getJSON()->setHttpResponseCode(\Magento\Framework\Webapi\Exception::HTTP_BAD_REQUEST);
            $this->getJSON()->setData(["Membership is already expired"]);

            return $this->getJSON();
        }

        $this->getJSON()->setData("{$this->getCustomer()->getFirstname()} {$this->getCustomer()->getLastname()}");

        return $this->getJSON();
    }
}