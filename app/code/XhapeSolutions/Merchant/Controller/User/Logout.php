<?php

namespace XhapeSolutions\Merchant\Controller\User;

class Logout extends AbstractAction
{
    protected function afterExecute()
    {
        unset($_SESSION['ulc_admin_sess_user_context_id']);

        return $this->getJSON();
    }
}