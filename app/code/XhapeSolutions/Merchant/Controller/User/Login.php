<?php

namespace XhapeSolutions\Merchant\Controller\User;

class Login extends AbstractAction
{
    protected function afterExecute()
    {
        if (isset($_SESSION['ulc_admin_sess_user_context_id']))
        {
            return $this->getJSON();
        }

        $strUsername = $this->getEscaper()->escapeHtml($this->getRequest()->getParam('username'));
        $strPassword = $this->getEscaper()->escapeHtml($this->getRequest()->getParam('password'));

        $this->getUser()->login($strUsername, $strPassword);

        $strRole = strtolower($this->getUser()->getRole()->getData('role_name'));

        if ($strRole !== 'merchant')
        {
            $this->getJSON()->setHttpResponseCode(\Magento\Framework\Webapi\Exception::HTTP_BAD_REQUEST);
        }
        else if (!isset($_SESSION['ulc_admin_sess_user_context_id']))
        {
            $_SESSION['ulc_admin_sess_user_context_id'] = $this->getUser()->getId();
        }

        return $this->getJSON();
    }
}