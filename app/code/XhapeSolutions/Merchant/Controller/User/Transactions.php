<?php

namespace XhapeSolutions\Merchant\Controller\User;

class Transactions extends AbstractAction
{
    private $objResourceConnection;

    protected $checkAuthorize = true;

    public function __construct(
        \Magento\Framework\App\Action\Context $objContext,
        \Magento\Framework\Controller\Result\JsonFactory $objJsonFactory,
        \Magento\Framework\Escaper $objEscaper,
        \Magento\User\Model\User $objUser,
        \Magento\Store\Model\StoreManagerInterface $objStoreManager,
        \Magento\Framework\App\ResourceConnection $objResourceConnection
    )
    {
        parent::__construct($objContext, $objJsonFactory, $objEscaper, $objUser, $objStoreManager);

        $this->objResourceConnection = $objResourceConnection;
    }

    protected function afterExecute()
    {
        $objConnection = $this->objResourceConnection->getConnection();
        $strSalesInvoiceItemTable = $this->objResourceConnection->getTableName('sales_invoice_item');
        $strJoinSalesInvoiceTable = $this->objResourceConnection->getTableName('sales_invoice');
        $strJoinOrderTable = $this->objResourceConnection->getTableName('sales_order');
        $strJoinCatalogProductVarcharTable = $this->objResourceConnection->getTableName('catalog_product_entity_varchar');
        $strJoinEavAttributeTable = $this->objResourceConnection->getTableName('eav_attribute');

        $strQuery = "SELECT t2.created_at AS dateInvoice, t2.increment_id AS invoiceId, t3.`status` AS orderStatus, t3.customer_firstname AS customerFirstName, t3.customer_lastname AS customerLastName, t1.name AS productName, t1.qty FROM {$strSalesInvoiceItemTable} AS t1 " .
                    "INNER JOIN {$strJoinSalesInvoiceTable} AS t2 ON t1.parent_id = t2.entity_id " .
                    "INNER JOIN {$strJoinOrderTable} AS t3 ON t3.entity_id = t2.order_id " .
                    "INNER JOIN {$strJoinCatalogProductVarcharTable} AS t4 ON t4.entity_id = t1.product_id " .
                    "INNER JOIN {$strJoinEavAttributeTable} AS t5 ON t5.attribute_id = t4.attribute_id " .
                    "WHERE t5.attribute_code = 'merchant_code' AND t4.value = '{$this->getUser()->getUserName()}' " .
                    "GROUP BY t4.entity_id, t1.entity_id " .
                    "ORDER BY t2.created_at DESC ";

        $arrResult = $objConnection->fetchAll($strQuery);

        if ($arrResult)
        {
            foreach($arrResult as &$arrInvoice)
            {
                $arrInvoice['dateInvoice'] = strtotime($arrInvoice['dateInvoice']) * 1000;
            }
        }

        $this->getJSON()->setData($arrResult);

        return $this->getJSON();
    }
}