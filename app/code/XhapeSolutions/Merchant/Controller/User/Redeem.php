<?php

namespace XhapeSolutions\Merchant\Controller\User;

class Redeem extends AbstractAction
{
    private $objProduct;
    private $objStockStateInterface;
    private $objQuoteManagement;
    private $objCustomerCollection;
    private $objCustomer;
    private $objQuoteFactory;
    private $objCustomerRepositoryInterface;
    private $objTransactionFactory;

    protected $checkAuthorize = true;

    public function __construct(
        \Magento\Framework\App\Action\Context $objContext,
        \Magento\Framework\Controller\Result\JsonFactory $objJsonFactory,
        \Magento\Framework\Escaper $objEscaper,
        \Magento\User\Model\User $objUser,
        \Magento\Store\Model\StoreManagerInterface $objStoreManager,
        \Magento\Catalog\Model\Product $objProduct,
        \Magento\CatalogInventory\Api\StockStateInterface $objStockStateInterface,
        \Magento\Customer\Model\ResourceModel\Customer\Collection $objCustomerCollection,
        \Magento\Customer\Model\Customer $objCustomer,
        \Magento\Customer\Api\CustomerRepositoryInterface $objCustomerRepositoryInterface,
        \Magento\Quote\Model\QuoteFactory $objQuoteFactory,
        \Magento\Quote\Model\QuoteManagement $objQuoteManagement,
        \Magento\Framework\DB\TransactionFactory $objTransactionFactory
    )
    {
        parent::__construct($objContext, $objJsonFactory, $objEscaper, $objUser, $objStoreManager);

        $this->objProduct = $objProduct;
        $this->objStockStateInterface = $objStockStateInterface;
        $this->objQuoteManagement = $objQuoteManagement;
        $this->objCustomerCollection = $objCustomerCollection;
        $this->objCustomer = $objCustomer;
        $this->objQuoteFactory = $objQuoteFactory;
        $this->objCustomerRepositoryInterface = $objCustomerRepositoryInterface;
        $this->objTransactionFactory = $objTransactionFactory;
    }

    private function addProduct()
    {
        $strMerchantCode = $this->getEscaper()->escapeHtml($this->getRequest()->getParam('sku'));

        $this->objProduct->load($this->objProduct->getIdBySku($strMerchantCode));

        return $this->objProduct->getId() > 0;
    }

    private function addCustomer()
    {
        $strMembershipCard = $this->getEscaper()->escapeHtml($this->getRequest()->getParam('membership_card'));

        $objCustomerCollection = $this->objCustomerCollection->addAttributeToFilter('membership_card_number', [
            'eq' => $strMembershipCard
        ])
            ->load()
            ->getData();

        if (isset($objCustomerCollection[0]))
        {
            $this->objCustomer->load($objCustomerCollection[0]['entity_id']);

            $dtMembershipCardExpiry = $this->objCustomer->getData('membership_card_expiry');

            if (is_string($dtMembershipCardExpiry))
            {
                return strtotime($dtMembershipCardExpiry);
            }
        }

        return false;
    }

    private function getCustomerAddresses()
    {
        $objAddresses = null;
        $arrAddresses = [];

        foreach ($this->objCustomer->getAddresses() as $objAddress)
        {
            $arrAddresses[] = $objAddress->toArray();
        }

        $objAddresses = isset($arrAddresses[0]) ? $arrAddresses[0] : $objAddresses;

        return [
            "region" => isset($objAddresses["region"]) ? $objAddresses["region"] : "",
            "region_id" => isset($objAddresses["region_id"]) ? $objAddresses["region_id"] : 0,
            "country_id" => isset($objAddresses["country_id"]) ? $objAddresses["country_id"] : 0,
            "street" => [
                isset($objAddresses["street"]) ? $objAddresses["street"] : ""
            ],
            "company" => "",
            "telephone" => isset($objAddresses["telephone"]) ? $objAddresses["telephone"] : "",
            "postcode" => isset($objAddresses["postcode"]) ? $objAddresses["postcode"] : "",
            "city" => isset($objAddresses["city"]) ? $objAddresses["city"] : "",
            "firstname" => $this->objCustomer->getFirstname(),
            "lastname" => $this->objCustomer->getLastname(),
            "email" => $this->objCustomer->getEmail(),
            "prefix" => "",
            "region_code" => ""
        ];
    }

    private function setOrder()
    {
        try
        {
            $objProduct = $this->objProduct;

            if ($objProduct)
            {
                $objQuote = $this->objQuoteFactory->create();
                $objQuote->setStore($this->getStoreManager()->getStore("certificates"));

                $objCustomerInterface = $this->objCustomerRepositoryInterface->getById($this->objCustomer->getEntityId());

                // Set item to quote
                $objQuote->assignCustomer($objCustomerInterface);
                $objQuote->addProduct($objProduct, 1);

                // Set addresses to quote
                $objQuote->getBillingAddress()->addData($this->getCustomerAddresses());
                $objQuote->getShippingAddress()->addData($this->getCustomerAddresses());

                $objShippingAddress = $objQuote->getShippingAddress();

                $objShippingAddress->setCollectShippingRates(true)
                    ->collectShippingRates()
                    ->setShippingMethod('freeshipping_freeshipping');

                // Save quote
                $objQuote->getPayment()->setMethod('checkmo');
                $objQuote->save();

                // Create Order From Quote
                $objOrder = $this->objQuoteManagement->submit($objQuote);

                $objOrder->setEmailSent(0);
                $objOrder->setCustomerNoteNotify(false);

                return $objOrder;
            }
        }
        catch (\Exception $objError)
        {
            /**
             * @todo: for now manually override the fix in mage 2.2 https://github.com/magento/magento2/commit/1fb3a97b4f394ae05d7afc5a213d0eb22d6e7c2e
             *
             * app/code/Magento/Quote/Model/Quote.php
             *
             * public function getItemById($itemId)
             * {
             *      //return $this->getItemsCollection()->getItemById($itemId);
             *      foreach ($this->getItemsCollection() as $item)
             *      {
             *          if ($item->getId() == $itemId)
             *          {
             *              return $item;
             *          }
             *      }
             *      return false;
             * }
             * */
            $this->getJSON()->setHttpResponseCode(\Magento\Framework\Webapi\Exception::HTTP_INTERNAL_ERROR);
            $this->getJSON()->setData([$objError->getMessage()]);
        }

        return false;
    }

    private function setInvoice(\Magento\Sales\Model\Order $objOrder)
    {
        $bolStatus = false;

        try
        {
            if(!$objOrder->canInvoice())
            {
                return $bolStatus;
            }

            $objInvoice = $objOrder->prepareInvoice();
            $objInvoice->setRequestedCaptureCase(\Magento\Sales\Model\Order\Invoice::CAPTURE_OFFLINE);
            $objInvoice->register();
            $objInvoice->getOrder()->setStatus(\Magento\Sales\Model\Order::STATE_COMPLETE);

            $objTransaction = $this->objTransactionFactory->create()
                ->addObject($objInvoice)
                ->addObject($objInvoice->getOrder());

            $objTransaction->save();

            $bolStatus = true;
        }
        catch (\Exception $objError)
        {
            $objOrder->setStatus(\Magento\Sales\Model\Order::STATE_PAYMENT_REVIEW);
            $objOrder->addStatusHistoryComment("Exception Message: {$objError->getMessage()}", false);
        }

        return $bolStatus;
    }

    protected function getCustomer()
    {
        return $this->objCustomer;
    }

    protected function getCustomerCollection()
    {
        return $this->objCustomerCollection;
    }

    protected function afterExecute()
    {
        if ($this->addProduct() === false)
        {
            $this->getJSON()->setHttpResponseCode(\Magento\Framework\Webapi\Exception::HTTP_NOT_FOUND);
            $this->getJSON()->setData(["Product not found"]);

            return $this->getJSON();
        }

        if ($this->getUser()->getUserName() !== $this->objProduct->getData('merchant_code'))
        {
            $this->getJSON()->setHttpResponseCode(\Magento\Framework\Webapi\Exception::HTTP_NOT_FOUND);
            $this->getJSON()->setData(["Product does not belong to merchant"]);

            return $this->getJSON();
        }

        if (($this->objStockStateInterface->getStockQty($this->objProduct->getId(), $this->objProduct->getStore()->getWebsiteId()) > 0) === false)
        {
            $this->getJSON()->setHttpResponseCode(\Magento\Framework\Webapi\Exception::HTTP_BAD_REQUEST);
            $this->getJSON()->setData(["Product out of stock"]);

            return $this->getJSON();
        }

        $intTimestampExpiry = $this->addCustomer();

        if ($intTimestampExpiry === false || ($intTimestampExpiry <= time()))
        {
            $this->getJSON()->setHttpResponseCode(\Magento\Framework\Webapi\Exception::HTTP_BAD_REQUEST);
            $this->getJSON()->setData(["Membership is already expired"]);

            return $this->getJSON();
        }

        $objOrder = $this->setOrder();

        if ($objOrder === false)
        {
            return $this->getJSON();
        }

        if ($this->setInvoice($objOrder) === false)
        {
            $arrErrorData = ["Failed to redeem certificate"];

            if ($objOrder->getEntityId())
            {
                if ($objOrder->canCancel())
                {
                    $arrErrorData[] = "Order #{$objOrder->getRealOrderId()} was cancelled";

                    $objOrder->cancel();
                }
                else
                {
                    $arrErrorData[] = "Order #{$objOrder->getRealOrderId()} for review";

                    $objOrder->setStatus(\Magento\Sales\Model\Order::STATE_PAYMENT_REVIEW);
                }

                $objOrder->save();

                $this->getJSON()->setHttpResponseCode(\Magento\Framework\Webapi\Exception::HTTP_BAD_REQUEST);
            }
            else
            {
                $this->getJSON()->setHttpResponseCode(\Magento\Framework\Webapi\Exception::HTTP_INTERNAL_ERROR);

                $arrErrorData[] = "Failed to create order";
            }

            $this->getJSON()->setData($arrErrorData);
        }
        else
        {
            $this->getJSON()->setData([$objOrder->getRealOrderId()]);
        }

        return $this->getJSON();
    }
}