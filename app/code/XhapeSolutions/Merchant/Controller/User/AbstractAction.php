<?php

namespace XhapeSolutions\Merchant\Controller\User;

abstract class AbstractAction extends \Magento\Framework\App\Action\Action
{
    private $objUser;
    private $objEscaper;
    private $objJson;
    private $objStoreManager;
    
    protected $checkAuthorize = false;

    public function __construct(
        \Magento\Framework\App\Action\Context $objContext,
        \Magento\Framework\Controller\Result\JsonFactory $objJsonFactory,
        \Magento\Framework\Escaper $objEscaper,
        \Magento\User\Model\User $objUser,
        \Magento\Store\Model\StoreManagerInterface $objStoreManager
    )
    {
        parent::__construct($objContext);

        $this->objEscaper = $objEscaper;
        $this->objUser = $objUser;
        $this->objJson = $objJsonFactory->create();
        $this->objStoreManager = $objStoreManager;

        if (!session_id())
        {
            session_start();
        }
    }
    
    abstract protected function afterExecute();
    
    protected function getJSON()
    {
        return $this->objJson;
    }
    
    protected function getEscaper()
    {
        return $this->objEscaper;
    }
    
    protected function getUser()
    {
        return $this->objUser;
    }
    
    protected function getStoreManager()
    {
        return $this->objStoreManager;
    }
    
    public function execute()
    {
        if ($this->checkAuthorize === true)
        {
            $bolResult = true;

            if (!isset($_SESSION['ulc_admin_sess_user_context_id']))
            {
                $bolResult = false;

                $this->objJson->setHttpResponseCode(\Magento\Framework\Webapi\Exception::HTTP_UNAUTHORIZED);
            }
            else
            {
                $this->objUser->load((int)$_SESSION['ulc_admin_sess_user_context_id']);

                $strRole = strtolower($this->objUser->getRole()->getData('role_name'));

                if ($strRole !== 'merchant')
                {
                    $bolResult = false;

                    $this->objJson->setHttpResponseCode(\Magento\Framework\Webapi\Exception::HTTP_FORBIDDEN);
                }
            }

            if ($bolResult === false)
            {
                return $this->objJson;
            }
        }
        
        return $this->afterExecute();
    }
}