<?php

namespace XhapeSolutions\Merchant\Controller\User;

class Products extends AbstractAction
{
    const CERT_SALT = "L@7DIC_f-tq\!3H";

    private $objStockStateInterface;

    protected $checkAuthorize = true;

    public function __construct(
        \Magento\Framework\App\Action\Context $objContext,
        \Magento\Framework\Controller\Result\JsonFactory $objJsonFactory,
        \Magento\Framework\Escaper $objEscaper,
        \Magento\User\Model\User $objUser,
        \Magento\Store\Model\StoreManagerInterface $objStoreManager,
        \Magento\CatalogInventory\Api\StockStateInterface $objStockStateInterface
    )
    {
        parent::__construct($objContext, $objJsonFactory, $objEscaper, $objUser, $objStoreManager);

        $this->objStockStateInterface = $objStockStateInterface;
    }

    // ref: http://devdocs.magento.com/guides/v2.0/rest/list.html
    // http://devdocs.magento.com/guides/v2.1/howdoi/webapi/search-criteria.html
    protected function afterExecute()
    {
        $strBaseUrl = $this->getStoreManager()->getStore()->getBaseUrl();

        // @fixme: only in localhost
        $strBaseUrl = str_replace(":7071", "", $strBaseUrl);

        $strCurl =  "{$strBaseUrl}rest/V1/products?searchCriteria[filter_groups][0][filters][0][field]=merchant_code&searchCriteria[filter_groups][0][filters][0][value]={$this->getUser()->getUserName()}";

        $objCurl = curl_init($strCurl);

        curl_setopt($objCurl, CURLOPT_HTTPHEADER, [
            "Authorization: Bearer " . getenv("ULC_MERCHANT_INTEGRATION_KEY")
        ]);

        curl_setopt($objCurl, CURLOPT_RETURNTRANSFER, true);

        $arrResult = [];
        $unkResult = curl_exec($objCurl);

        if ($unkResult !== false)
        {
            $unkResult = json_decode($unkResult, true);

            if (isset($unkResult['items']))
            {
                $strCatalogMediaUrl = $this->getStoreManager()->getStore()->getBaseUrl(\Magento\Framework\UrlInterface::URL_TYPE_MEDIA) . "catalog/product";

                foreach($unkResult['items'] as $arrItem)
                {
                    $strSku = $arrItem['sku'];
                    $intQty = $this->objStockStateInterface->getStockQty($arrItem['id']);
                    $strName = $arrItem['name'];
                    $strImage = null;
                    $strLink = "/certificates/{$strSku}.html";

                    if (isset($arrItem['custom_attributes']))
                    {
                        foreach($arrItem['custom_attributes'] as $arrAttributes)
                        {
                            if (isset($arrAttributes['attribute_code']) && $arrAttributes['attribute_code'] === 'image')
                            {
                                $strImage = "{$strCatalogMediaUrl}{$arrAttributes['value']}";
                            }
                        }
                    }

                    $arrResult[] = [
                        'qty' => $intQty,
                        'sku' => $strSku,
                        'code' => crypt($strSku, self::CERT_SALT),
                        'name' => $strName,
                        'image' => $strImage,
                        'url' => $strLink
                    ];
                }
            }
        }

        curl_close($objCurl);

        $this->getJSON()->setData($arrResult);

        return $this->getJSON();
    }
}