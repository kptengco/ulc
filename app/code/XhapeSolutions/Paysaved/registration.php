<?php
/**
 * @author     XhapeSolutions
 * @copyright  2018 XhapeSolutions
 */
\Magento\Framework\Component\ComponentRegistrar::register(
    \Magento\Framework\Component\ComponentRegistrar::MODULE,
    'XhapeSolutions_Paysaved',
    __DIR__
);