<?php

namespace XhapeSolutions\Paysaved\Controller\Process;

class Transaction extends Payment
{
    private $objPaymentpending;
    private $objPaymentsuccess;

    public function __construct(
        \Magento\Framework\App\Action\Context $objContext,
        \Magento\Framework\Controller\Result\JsonFactory $objJsonFactory,
        \Magento\Catalog\Model\Product $objProduct,
        \Magento\Checkout\Model\Cart $objCart,
        \Magento\Quote\Model\QuoteManagement $objQuoteManagement,
        \Magento\Framework\Escaper $objEscaper,
        \Magento\Store\Model\StoreManagerInterface $objStoreManager,
        \Magento\Framework\DB\TransactionFactory $objTransactionFactory,
        \Magento\Sales\Model\Order $objOrder,
        Paymentpending $objPaymentpending,
        Paymentsuccess $objPaymentsuccess
    )
    {
        parent::__construct(
            $objContext,
            $objJsonFactory,
            $objProduct,
            $objCart,
            $objQuoteManagement,
            $objEscaper,
            $objStoreManager,
            $objTransactionFactory,
            $objOrder
        );

        $this->objPaymentpending = $objPaymentpending;
        $this->objPaymentsuccess = $objPaymentsuccess;
    }

    public function execute()
    {
        try
        {
            $arrResponse = $this->getPaySavedResponse();

            if (!empty($arrResponse))
            {
                switch ($this->getPaySavedOrderStatus())
                {
                    case "pending":
                        $this->objPaymentpending->execute();
                        break;
                    case "completed":
                        $this->objPaymentsuccess->execute();
                        break;
                }
            }
        }
        catch (\Exception $objError)
        {

        }
    }
}