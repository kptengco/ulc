<?php

namespace XhapeSolutions\Paysaved\Controller\Process;

class Paymentpending extends Payment
{
    public function execute()
    {
        try
        {
            $arrResponse = $this->getPaySavedResponse();

            if (!empty($arrResponse))
            {
                $intOrderId = $this->getPaySavedOrderId();

                if ($intOrderId)
                {
                    $this->getOrder()->load($intOrderId);

                    $this->getOrder()->setStatus(\Magento\Sales\Model\Order::STATE_PENDING_PAYMENT);

                    $this->getOrder()->addStatusHistoryComment("Paysaved Pending Response: " . json_encode($arrResponse)); //save paysaved response

                    $this->getOrder()->save();
                }
            }
        }
        catch (\Exception $objError)
        {

        }
    }
}