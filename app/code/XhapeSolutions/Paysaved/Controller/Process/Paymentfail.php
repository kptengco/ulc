<?php

namespace XhapeSolutions\Paysaved\Controller\Process;

class Paymentfail extends PaymentProductUpdate
{
    public function execute()
    {
        try
        {
            $arrResponse = $this->getPaySavedResponse();

            if (!empty($arrResponse))
            {
                $intOrderId = $this->getPaySavedOrderId();

                if ($intOrderId)
                {
                    $this->getOrder()->load($intOrderId);

                    $this->addProductQty();

                    $this->getOrder()->setStatus(\Magento\Sales\Model\Order::STATE_PAYMENT_REVIEW);

                    $strError = "Order failed.";

                    $this->getOrder()->addStatusHistoryComment($strError);
                    $this->getOrder()->addStatusHistoryComment(json_encode($arrResponse)); //save paymaya response

                    $this->getOrder()->save();
                }
            }
        }
        catch (\Exception $objError)
        {

        }
    }
}