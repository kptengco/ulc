<?php

namespace XhapeSolutions\Paysaved\Controller\Process;

class Paymentcancel extends PaymentProductUpdate
{
    public function execute()
    {
        try
        {
            $arrResponse = $this->getPaySavedResponse();

            if (!empty($arrResponse))
            {
                $intOrderId = $this->getPaySavedOrderId();

                if ($intOrderId)
                {
                    $this->getOrder()->load($intOrderId);

                    $this->addProductQty();

                    if ($this->getOrder()->canCancel())
                    {
                        /**
                         * @readme: Err: Order Cancelled
                         * - The charge failed
                         *
                         * */
                        $this->getOrder()->cancel();

                        $strError = "Order cancelled.";
                    }
                    else
                    {
                        $this->getOrder()->setStatus(\Magento\Sales\Model\Order::STATE_PAYMENT_REVIEW);

                        $strError = "Order cannot be cancelled.";
                    }

                    $this->getOrder()->addStatusHistoryComment($strError);
                    $this->getOrder()->addStatusHistoryComment(json_encode($arrResponse)); //save paysaved response

                    $this->getOrder()->save();
                }
            }
        }
        catch (\Exception $objError)
        {

        }
    }
}