Ref: https://ranasohel.me/2017/01/16/generate-invoice-and-shipment-automatically-when-a-new-order-is-placed-in-magento-2/

php bin/magento setup:upgrade
php bin/magento cache:flush (if you had cache enabled)
php bin/magento indexer:reindex

To enable:
php bin/magento module:enable --clear-static-content XhapeSolutions_MembershipOrder

If theme messed up:

php bin/magento indexer:reindex
php bin/magento cache:flush

remove folders:
/pub/static/frontend
/pub/static/adminhtml
/var/cache
/var/view_preprocessed

php -dmemory_limit=3G bin/magento setup:static-content:deploy && php bin/magento cache:flush

update .htaccess in /pub/static/.htaccess and copy content from https://github.com/magento/magento2/blob/2.2-develop/pub/static/.htaccess

##### for production deploy
php -dmemory_limit=3G bin/magento deploy:mode:set production

##### if deploy failed
php -dmemory_limit=3G bin/magento setup:di:compile
php -dmemory_limit=3G bin/magento setup:static-content:deploy

##### disable maintenance mode
rm var/.maintenance.flag
or 
bin/magento maintenance:disable

##### to change base url via cli
php bin/magento setup:store-config:set --base-url="http://localhost:8080/"
php bin/magento setup:store-config:set --base-url-secure="https://localhost:8080/"
php bin/magento cache:flush

#### bug fix in layout update xml
vendor/magento/framework/View/Layout/etc/page_layout.xsd

Need to add :

    <xs:include schemaLocation="urn:magento:framework:View/Layout/etc/head.xsd"/>
    <xs:include schemaLocation="urn:magento:framework:View/Layout/etc/body.xsd"/>

            <xs:element name="head" type="headType" minOccurs="0" maxOccurs="unbounded"/>
            <xs:element name="body" type="bodyType" minOccurs="0" maxOccurs="unbounded"/>
            
       
Complete file :
   
<?xml version="1.0" encoding="UTF-8"?>
<!--
/**
 * Copyright © 2013-2017 Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
-->
<xs:schema xmlns:xs="http://www.w3.org/2001/XMLSchema">
    <xs:include schemaLocation="urn:magento:framework:View/Layout/etc/elements.xsd"/>
    <xs:include schemaLocation="urn:magento:framework:View/Layout/etc/head.xsd"/>
    <xs:include schemaLocation="urn:magento:framework:View/Layout/etc/body.xsd"/>

       <xs:complexType name="pageLayoutType">
           <xs:sequence minOccurs="0" maxOccurs="unbounded">
               <xs:element ref="referenceContainer" minOccurs="0" maxOccurs="unbounded"/>
               <xs:element ref="container" minOccurs="0" maxOccurs="unbounded"/>
               <xs:element ref="update" minOccurs="0" maxOccurs="unbounded"/>
               <xs:element ref="move" minOccurs="0" maxOccurs="unbounded"/>
               <xs:element name="head" type="headType" minOccurs="0" maxOccurs="unbounded"/>
               <xs:element name="body" type="bodyType" minOccurs="0" maxOccurs="unbounded"/>
           </xs:sequence>
       </xs:complexType>
   
       <xs:element name="layout" type="pageLayoutType">
           <xs:unique name="containerKey">
               <xs:selector xpath=".//container"/>
               <xs:field xpath="@name"/>
           </xs:unique>
       </xs:element>
   </xs:schema>
   
   
###### deploy in prod script
php bin/magento maintenance:enable && 
php bin/magento setup:upgrade && 
php -dmemory_limit=3G bin/magento setup:di:compile &&
php -dmemory_limit=3G bin/magento setup:static-content:deploy && 
php bin/magento cache:flush && 
php bin/magento indexer:reindex && 
php bin/magento maintenance:disable

###### add disable/enable customer registration plugin
composer config repositories.magento2-disable-customer-registration vcs https://github.com/deved-it/magento2-disable-customer-registration

composer require deved/magento2-disable-customer-registration:dev-master

php bin/magento setup:upgrade

##### update composer memory_limit
which composer (copy path)

php -d memory_limit=3G /opt/cpanel/composer/bin/composer (args)


##### localhost
php bin/magento setup:upgrade && 
php -dmemory_limit=3G bin/magento setup:static-content:deploy --theme XhapeSolutions/ulc && 
php bin/magento cache:flush && 
php bin/magento indexer:reindex

##### set admin via cli
php bin/magento admin:user:create --admin-user="kptengco" --admin-password="abcd1234" --admin-email="kptengco@example.com" --admin-firstname="Kenneth" --admin-lastname="Tengco"