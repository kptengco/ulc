<?php

namespace XhapeSolutions\MembershipOrder\Controller\Order2;

use PayMaya\PayMayaSDK;
use PayMaya\API\Checkout;
use PayMaya\Model\Checkout\Buyer;
use PayMaya\Model\Checkout\ItemAmountDetails;
use PayMaya\Model\Checkout\ItemAmount;
use PayMaya\Model\Checkout\Item;
use PayMaya\Model\Checkout\Contact;
use PayMaya\Model\Checkout\Address;
use PayMaya\API\Webhook;

class Create extends Payment
{
    public function execute()
    {
        try
        {
            if (!$this->validateFormRequest())
            {
                return $this->getJSON();
            }

            $this->getProduct()->load($this->getProduct()->getIdBySku('membership_card'));

            $objOrder = $this->checkout();

            $strCheckoutUrl = $this->createOrder($objOrder);

            $this->getJSON()->setData($strCheckoutUrl);
        }
        catch(\Exception $objError)
        {
            $this->addLogger([
                $objError->getMessage()
            ]);
        }

        return $this->getJSON();
    }

    private function createOrder($objOrder)
    {
        try
        {
            if (!session_id())
            {
                session_start();
            }

            if (!empty($_POST))
            {
                $strResult = $this->generateCheckoutUrl($objOrder->getEntityId());

                if ($strResult)
                {
                    return $strResult;
                }
                else
                {
                    throw new \Exception("Err: Generating checkout");
                }
            }
            else
            {
                throw new \Exception("Err: No Data");
            }
        }
        catch (\Exception $objError)
        {
            $this->addLogger([$objError->getMessage()]);
        }

        return false;
    }

    /**
     * @readme: Generate PayMaya checkout
     *
     * @return $strUri
     *
     * */
    private function generateCheckoutUrl($intOrderRef)
    {
        PayMayaSDK::getInstance()->initCheckout(self::$PUBLIC_KEY, self::$API_KEY, self::$PAYMAYA_ENV);

        // Checkout
        $objCheckout = new Checkout();

        // Contact
        $objContact = new Contact();
        $objContact->phone = $this->getEscaper()->escapeHtml($this->getRequest()->getParam('mobileNumber'));
        $objContact->email = $this->getEscaper()->escapeHtml($this->getRequest()->getParam('email'));

        // Address
        $objAddress = new Address();
        $objAddress->line1 = $this->getEscaper()->escapeHtml($this->getRequest()->getParam('address'));
        $objAddress->line2 = "";
        $objAddress->city = $this->getEscaper()->escapeHtml($this->getRequest()->getParam('city'));
        $objAddress->state = "";
        $objAddress->zipCode = $this->getEscaper()->escapeHtml($this->getRequest()->getParam('postalCode'));
        $objAddress->countryCode = $this->getEscaper()->escapeHtml($this->getRequest()->getParam('country'));

        $objBuyer = new Buyer();
        $objBuyer->firstName = $this->getEscaper()->escapeHtml($this->getRequest()->getParam('firstName'));
        $objBuyer->middleName = null;
        $objBuyer->lastName = $this->getEscaper()->escapeHtml($this->getRequest()->getParam('lastName'));
        $objBuyer->contact = $objContact;
        $objBuyer->shippingAddress = $objAddress;
        $objBuyer->billingAddress = $objAddress;

        $objCheckout->buyer = $objBuyer;

        // Item
        $objItemAmountDetails = new ItemAmountDetails();
        $objItemAmountDetails->shippingFee = "0";
        $objItemAmountDetails->tax = "0";
        $objItemAmountDetails->subtotal = $this->getProduct()->getPrice();

        $objItemAmount = new ItemAmount();
        $objItemAmount->currency = "PHP";
        $objItemAmount->value = number_format($this->getProduct()->getPrice(), 2, ".", "");
        $objItemAmount->details = $objItemAmountDetails;

        $strItemDesc = $this->getProduct()->getDescription();
        $strItemDescription = trim($strItemDesc) ? substr(strip_tags($strItemDesc), 1,255) : "no description";

        $objItem = new Item();
        $objItem->name = $this->getProduct()->getName();
        $objItem->code = $this->getProduct()->getSku();
        $objItem->description = $strItemDescription;
        $objItem->quantity = "1";
        $objItem->amount = $objItemAmount;
        $objItem->totalAmount = $objItemAmount;

        $objCheckout->items = array($objItem);
        $objCheckout->totalAmount = $objItemAmount;
        $objCheckout->requestReferenceNumber = (string)$intOrderRef;
        $objCheckout->redirectUrl = array(
            "success" => $this->getStoreBaseUrl() . "membership-card-transaction?status=success&o=" . (str_pad($intOrderRef,10,"0",STR_PAD_LEFT)),
            "failure" => $this->getStoreBaseUrl() . "membership-card-transaction?status=fail",
            "cancel" => $this->getStoreBaseUrl() . "membership-card-transaction?status=cancel"
        );

        $objCheckout->execute();

        $this->registerIPN();

        return $objCheckout->url; // Checkout URL
    }

    private function checkout()
    {
        try
        {
            $objProduct = $this->getProduct();

            if ($objProduct)
            {
                $arrShippingAddress = [
                    "region" => "",
                    "region_id" => 0,
                    "country_id" => $this->getEscaper()->escapeHtml($this->getRequest()->getParam('country')),
                    "street" => [
                        $this->getEscaper()->escapeHtml($this->getRequest()->getParam('address'))
                    ],
                    "company" => "",
                    "telephone" => $this->getEscaper()->escapeHtml($this->getRequest()->getParam('phoneNumber')),
                    "postcode" => $this->getEscaper()->escapeHtml($this->getRequest()->getParam('postalCode')),
                    "city" => $this->getEscaper()->escapeHtml($this->getRequest()->getParam('city')),
                    "firstname" => $this->getEscaper()->escapeHtml($this->getRequest()->getParam('firstName')),
                    "lastname" => $this->getEscaper()->escapeHtml($this->getRequest()->getParam('lastName')),
                    "email" => $this->getEscaper()->escapeHtml($this->getRequest()->getParam('email')),
                    "prefix" => "",
                    "region_code" => ""
                ];

                $this->getCart()->truncate();
                $this->getCart()->addProduct($objProduct, [
                    'qty' => 1
                ]);

                $objQuote = $this->getCart()->getQuote();
                $objQuote->setStore($this->getStoreManager()->getStore("membership_store_view"));

                //Set Customer
                $objQuote->setCustomerEmail($this->getEscaper()->escapeHtml($this->getRequest()->getParam('email')));
                $objQuote->setCustomerIsGuest(true);

                //Set Address to quote
                $objQuote->getBillingAddress()->addData($arrShippingAddress);

                $objShippingAddress = $objQuote
                    ->getShippingAddress()
                    ->addData($arrShippingAddress);

                $objShippingRates = $objShippingAddress->collectShippingRates();
                $objShippingRates->setShippingMethod('freeshipping_freeshipping');

                // Set Sales Order Payment
                $objQuote->getPayment()->setMethod('checkmo');

                $this->getCart()->save();

                // Create Order From Quote
                $objOrder = $this->getQuoteManagement()->submit($objQuote);

                $objOrder->setEmailSent(0);
                $objOrder->setCustomerNoteNotify(false);

                $objOrder->addStatusHistoryComment(
                    "Partner Code: {$this->getEscaper()->escapeHtml($this->getRequest()->getParam('partnerCode'))}, " .
                    "Gender: {$this->getEscaper()->escapeHtml($this->getRequest()->getParam('gender'))}, " .
                    "Birthday: {$this->getEscaper()->escapeHtml($this->getRequest()->getParam('birthday'))}"
                );

                $objOrder->save();

                return $objOrder;
            }
        }
        catch (\Exception $objError)
        {
            /**
             * @todo: for now manually override the fix in mage 2.2 https://github.com/magento/magento2/commit/1fb3a97b4f394ae05d7afc5a213d0eb22d6e7c2e
             *
             * app/code/Magento/Quote/Model/Quote.php
             *
             * public function getItemById($itemId)
             * {
             *      //return $this->getItemsCollection()->getItemById($itemId);
             *      foreach ($this->getItemsCollection() as $item)
             *      {
             *          if ($item->getId() == $itemId)
             *          {
             *              return $item;
             *          }
             *      }
             *      return false;
             * }
             * */
            throw new \Exception($objError->getMessage());
        }

        return false;
    }

    /**
     * @readme: Handle Form Data
     *
     * @return boolean
     *
     **/
    private function validateFormRequest()
    {
        $arrError = [];

        $arrPost = $this->getRequest();
        $objEscaper = $this->getEscaper();

        if (!$objEscaper->escapeHtml($arrPost->getParam('email')))
        {
            $arrError[] = 'Email is required';
        }

        if (!$objEscaper->escapeHtml($arrPost->getParam('firstName')))
        {
            $arrError[] = 'First Name is required';
        }

        if (strlen($objEscaper->escapeHtml($arrPost->getParam('firstName'))) > 45)
        {
            $arrError[] = 'First Name should be less than 45 characters';
        }

        if (!$objEscaper->escapeHtml($arrPost->getParam('lastName')))
        {
            $arrError[] = 'Last Name is required';
        }

        if (strlen($objEscaper->escapeHtml($arrPost->getParam('lastName'))) > 45)
        {
            $arrError[] = 'Last Name should be less than 45 characters';
        }

        if (!$objEscaper->escapeHtml($arrPost->getParam('address')))
        {
            $arrError[] = 'Street Address is required';
        }

        if (strlen($objEscaper->escapeHtml($arrPost->getParam('address'))) > 150)
        {
            $arrError[] = 'Street Address should be less than 150 characters';
        }

        if (!$objEscaper->escapeHtml($arrPost->getParam('city')))
        {
            $arrError[] = 'City is required';
        }

        if (strlen($objEscaper->escapeHtml($arrPost->getParam('city'))) > 100)
        {
            $arrError[] = 'City should be less than 100 characters';
        }

        if (!$objEscaper->escapeHtml($arrPost->getParam('postalCode')))
        {
            $arrError[] = 'Postal/Zip code is required';
        }

        if (strlen($objEscaper->escapeHtml($arrPost->getParam('postalCode'))) > 20)
        {
            $arrError[] = 'Postal/Zip code should be less than 20 characters';
        }

        if (!$objEscaper->escapeHtml($arrPost->getParam('country')))
        {
            $arrError[] = 'Country is required';
        }

        if (strlen($objEscaper->escapeHtml($arrPost->getParam('country'))) > 2)
        {
            $arrError[] = 'Country should be less than 2 characters';
        }

        if (!$objEscaper->escapeHtml($arrPost->getParam('phoneNumber')))
        {
            $arrError[] = 'Phone number is required';
        }

        if (strlen($objEscaper->escapeHtml($arrPost->getParam('phoneNumber'))) > 25)
        {
            $arrError[] = 'Phone number should be less than 25 characters';
        }

        if (!empty($arrError))
        {
            $this->addLogger($arrError);

            return false;
        }

        return true;
    }

    private function registerIPN()
    {
        if (self::$PAYMAYA_ENV == "SANDBOX")
        {
            $arrHooks = Webhook::retrieve();

            foreach($arrHooks as $objHook)
            {
                $objHook->delete();
            }
        }

        $objWebhook = new Webhook();
        $objWebhook->name = Webhook::CHECKOUT_SUCCESS;
        $objWebhook->callbackUrl = $this->getStoreBaseUrl() . "paymaya/process/paymentsuccess";
        $objWebhook->register();

        // cancelled
        $objWebhook = new Webhook();
        $objWebhook->name = "CHECKOUT_DROPOUT";
        $objWebhook->callbackUrl = $this->getStoreBaseUrl() . "paymaya/process/paymentcancel";
        $objWebhook->register();

        $objWebhook = new Webhook();
        $objWebhook->name = Webhook::CHECKOUT_FAILURE;
        $objWebhook->callbackUrl = $this->getStoreBaseUrl() . "paymaya/process/paymentfail";
        $objWebhook->register();
    }
}