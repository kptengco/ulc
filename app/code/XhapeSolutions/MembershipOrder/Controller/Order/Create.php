<?php

namespace XhapeSolutions\MembershipOrder\Controller\Order;

use MagpieApi\Magpie;

class Create extends \Magento\Framework\App\Action\Action
{
    private static $MAGPIE_PUBLIC_KEY;
    private static $MAGPIE_SECRET_KEY;

    private $objJsonFactory;
    private $objJson;
    private $objProduct;
    private $objCart;
    private $objQuoteManagement;
    private $objTransactionFactory;
    private $objEscaper;

    public function __construct(\Magento\Framework\App\Action\Context $objContext,
                                \Magento\Framework\Controller\Result\JsonFactory $objJsonFactory,
                                \Magento\Catalog\Model\Product $objProduct,
                                \Magento\Checkout\Model\Cart $objCart,
                                \Magento\Quote\Model\QuoteManagement $objQuoteManagement,
                                \Magento\Framework\DB\TransactionFactory $objTransactionFactory,
                                \Magento\Framework\Escaper $objEscaper
    )
    {
        parent::__construct($objContext);

        self::$MAGPIE_PUBLIC_KEY = getenv('MAGPIE_PUBLIC_KEY');
        self::$MAGPIE_SECRET_KEY = getenv('MAGPIE_SECRET_KEY');

        $this->objJsonFactory = $objJsonFactory;
        $this->objProduct = $objProduct;
        $this->objCart = $objCart;
        $this->objQuoteManagement = $objQuoteManagement;
        $this->objTransactionFactory = $objTransactionFactory;
        $this->objEscaper = $objEscaper;

        $this->objJson = $this->objJsonFactory->create();
    }

    static public function getMagpiePublicKey()
    {
        return getenv('MAGPIE_PUBLIC_KEY');
    }

    public function execute()
    {
        try
        {
            if (!$this->validateFormRequest())
            {
                return $this->objJson;
            }

            $this->objProduct->load($this->objProduct->getIdBySku('membership_card'));

            $objResponse = $this->processOrder();

            if ($objResponse)
            {
                $arrLog = [];
                $objOrder = $this->checkout();

                if ($objOrder)
                {
                    // Get the reply from Magpie
                    $arrReply = $objResponse->toArray();

                    // Check the response
                    if ($objResponse->isSuccess() && $objOrder->getEntityId() && $this->invoice($objOrder, $arrReply))
                    {
                        // The charge went through
                        // TO-DO:
                        // Record the transaction details in your database of choice
                        // Send an email receipt to your customer
                        // Other stuff that you need to do
                        $intOrderId = $objOrder->getRealOrderId();

                        $this->objJson->setData($intOrderId);
                    }
                    else if ($objOrder->canCancel())
                    {
                        /**
                         * @readme: Err: Order Cancelled
                         * - The charge failed
                         *
                         * */
                        $objOrder->cancel();

                        $arrLog[] = 'Err: Payment Error';

                        if (!$objOrder->getEntityId())
                        {
                            $arrLog[] = 'Failed to save order';
                        }
                    }
                    else
                    {
                        /**
                         * @readme: Err: Order cannot be cancelled
                         *
                         * */

                        $arrLog[] = 'Err: Order cannot be cancelled';

                        $objOrder->setStatus(\Magento\Sales\Model\Order::STATE_PAYMENT_REVIEW);
                    }

                    $objOrder->addStatusHistoryComment("Gender: {$this->objEscaper->escapeHtml($this->getRequest()->getParam('gender'))}, Birthday: {$this->objEscaper->escapeHtml($this->getRequest()->getParam('birthday'))}");

                    $objOrder->addStatusHistoryComment(json_encode($arrReply)); //save magpie response

                    if (!empty($arrLog))
                    {
                        $objOrder->addStatusHistoryComment("Err: " . json_encode($arrLog));
                        $this->addLogger($arrLog);
                    }

                    $objOrder->save();
                }
            }
        }
        catch(\Exception $objError)
        {
            $this->addLogger([
                $objError->getMessage()
            ]);
        }

        return $this->objJson;
    }

    private function processOrder()
    {
        try
        {
            if (!session_id())
            {
                session_start();
            }

            if (!empty($_POST))
            {
                $strToken = $this->validateToken();

                if ($strToken)
                {
                    $intAmount = $this->validateAmount();

                    if ($intAmount)
                    {
                        $strDescription = $this->validateDescription();

                        if ($strDescription)
                        {
                            return $this->makePayment($strToken, $intAmount, $strDescription);
                        }
                    }
                }
            }
            else
            {
                $this->addLogger([
                    'Err: No Data'
                ]);
            }
        }
        catch (\Exception $objError)
        {
            $this->addLogger([$objError->getMessage()]);
        }

        return false;
    }

    /**
     * @readme: Let's bake some pie
     *
     * @param $strToken string
     * @param $intAmount integer
     * @param $strDescription string
     *
     * @return $objResponse \Magpie\Response
     *
     * */
    private function makePayment($strToken = null, $intAmount = 0, $strDescription = null)
    {
        $objMagpie = new Magpie(self::$MAGPIE_PUBLIC_KEY, self::$MAGPIE_SECRET_KEY);

        // Create the charge and capture
        $objResponse = $objMagpie->charge->create(
            $intAmount, // Amount
            'php', // Currency
            $strToken, // Token
            $strDescription, // Product description
            'ULC', // The statement descriptor
            true // Capture (true is capture, false is not)
        );

        return $objResponse;
    }

    private function checkout()
    {
        try
        {
            $objProduct = $this->objProduct;

            if ($objProduct)
            {
                $arrShippingAddress = [
                    "region" => "",
                    "region_id" => 0,
                    "country_id" => $this->objEscaper->escapeHtml($this->getRequest()->getParam('country')),
                    "street" => [
                        $this->objEscaper->escapeHtml($this->getRequest()->getParam('address'))
                    ],
                    "company" => "",
                    "telephone" => $this->objEscaper->escapeHtml($this->getRequest()->getParam('phoneNumber')) . " / " . $this->objEscaper->escapeHtml($this->getRequest()->getParam('mobileNumber')),
                    "postcode" => $this->objEscaper->escapeHtml($this->getRequest()->getParam('postalCode')),
                    "city" => $this->objEscaper->escapeHtml($this->getRequest()->getParam('city')),
                    "firstname" => $this->objEscaper->escapeHtml($this->getRequest()->getParam('firstName')),
                    "lastname" => $this->objEscaper->escapeHtml($this->getRequest()->getParam('lastName')),
                    "email" => $this->objEscaper->escapeHtml($this->getRequest()->getParam('email')),
                    "prefix" => "",
                    "region_code" => ""
                ];

                $this->objCart->truncate();
                $this->objCart->addProduct($objProduct, [
                    'qty' => 1
                ]);

                $objQuote = $this->objCart->getQuote();

                //Set Customer
                $objQuote->setCustomerEmail($this->objEscaper->escapeHtml($this->getRequest()->getParam('email')));
                $objQuote->setCustomerIsGuest(true);

                //Set Address to quote
                $objQuote->getBillingAddress()->addData($arrShippingAddress);

                $objShippingAddress = $objQuote
                    ->getShippingAddress()
                    ->addData($arrShippingAddress);

                $objShippingRates = $objShippingAddress->collectShippingRates();
                $objShippingRates->setShippingMethod('freeshipping_freeshipping');

                // Set Sales Order Payment
                $objQuote->getPayment()->setMethod('checkmo');

                $this->objCart->save();

                // Create Order From Quote
                $objOrder = $this->objQuoteManagement->submit($objQuote);

                $objOrder->setEmailSent(0);
                $objOrder->setCustomerNoteNotify(false);

                return $objOrder;
            }
        }
        catch (\Exception $objError)
        {
            /**
             * @todo: for now manually override the fix in mage 2.2 https://github.com/magento/magento2/commit/1fb3a97b4f394ae05d7afc5a213d0eb22d6e7c2e
             *
             * app/code/Magento/Quote/Model/Quote.php
             *
             * public function getItemById($itemId)
             * {
             *      //return $this->getItemsCollection()->getItemById($itemId);
             *      foreach ($this->getItemsCollection() as $item)
             *      {
             *          if ($item->getId() == $itemId)
             *          {
             *              return $item;
             *          }
             *      }
             *      return false;
             * }
             * */
            $this->addLogger([
                $objError->getMessage(),
            ]);
        }

        return false;
    }

    private function invoice(\Magento\Sales\Model\Order $objOrder, $arrMagpieReply = [])
    {
        $bolStatus = false;

        try
        {
            if(!$objOrder->canInvoice())
            {
                return $bolStatus;
            }

            $objInvoice = $objOrder->prepareInvoice();
            $objInvoice->setRequestedCaptureCase(\Magento\Sales\Model\Order\Invoice::CAPTURE_OFFLINE);
            $objInvoice->register();
            $objInvoice->getOrder()->setStatus(\Magento\Sales\Model\Order::STATE_COMPLETE);
            $objInvoice->addComment(json_encode($arrMagpieReply));

            $objTransaction = $this->objTransactionFactory->create()
                ->addObject($objInvoice)
                ->addObject($objInvoice->getOrder());

            $objTransaction->save();

            $bolStatus = true;
        }
        catch (\Exception $objError)
        {
            $objOrder->setStatus(\Magento\Sales\Model\Order::STATE_PAYMENT_REVIEW);
            $objOrder->addStatusHistoryComment("Exception Message: {$objError->getMessage()}", false);

            $this->addLogger([
                $objError->getMessage()
            ]);
        }

        return $bolStatus;
    }

    private function getAttackerIPAddr()
    {
        if (isset($_SERVER['HTTP_CLIENT_IP']) && $_SERVER['HTTP_CLIENT_IP'] != null)
        {
            return $_SERVER['HTTP_CLIENT_IP'];
        }
        else if (isset($_SERVER['HTTP_X_FORWARDED_FOR']) && $_SERVER['HTTP_X_FORWARDED_FOR'] != null)
        {
            return $_SERVER['HTTP_X_FORWARDED_FOR'];
        }
        else
        {
            return $_SERVER['REMOTE_ADDR'];
        }
    }

    /**
     * @readme: Handle token
     *
     * @return $strToken string
     *
     **/
    private function validateToken()
    {
        $strToken = null;

        if ($this->objEscaper->escapeHtml($this->getRequest()->getParam('token')))
        {
            $strToken = $this->objEscaper->escapeHtml($this->getRequest()->getParam('token'));

            // Check for a duplicate submission, just in case:
            // Uses sessions, you could use a cookie instead.
            if (isset($_SESSION['token']) && ($_SESSION['token'] == $strToken))
            {
                $strToken = null;

                /**
                 * @readme: Err: HTTP Re-use
                 * - Form Submission Error
                 * - You have apparently resubmitted the form. Please do not do that.
                 *
                 * */
                $this->addLogger([
                    'Err: Token Re-use'
                ]);
            }
            else // New submission.
            {
                $_SESSION['token'] = $strToken;

                $strToken = $_SESSION['token'];
            }
        }
        else
        {
            /**
             * @readme: Err: Token Missing
             * - JavaScript Error
             * - The order cannot be processed. Please make sure you have JavaScript enabled and try again.
             *
             * */
            $this->addLogger([
                'Err: Token Missing'
            ]);
        }

        return $strToken;
    }

    /**
     * @readme: Handle amount
     *
     * @return $intAmount integer
     *
     **/
    private function validateAmount()
    {
        $intAmount = 0;

        $intPostAmount = $this->objEscaper->escapeHtml($this->getRequest()->getParam('amount'));

        if ($intPostAmount && ($this->objProduct->getPrice() == $intPostAmount))
        {
            $intAmount = $this->objProduct->getPrice();
        }
        else
        {
            /**
             * @readme: Err: Amount Tampered
             * - Payment Error
             * - Amount is not equal to the expected ordered amount.
             *
             * */
            $this->addLogger([
                'Err: Amount Tampered'
            ]);
        }

        return $intAmount;
    }

    /**
     * @readme: Handle description
     *
     * @return $strDescription string
     *
     **/
    private function validateDescription()
    {
        $strDescription = '';

        if ($this->objEscaper->escapeHtml($this->getRequest()->getParam('description')))
        {
            $strDescription = $this->objEscaper->escapeHtml($this->getRequest()->getParam('description'));
        }
        else
        {
            /**
             * @readme: Err: Unknown Item
             * - Purchase Error
             * - Item not found
             *
             * */
            $this->addLogger([
                'Err: Unknown Item'
            ]);
        }

        return $strDescription;
    }

    /**
     * @readme: Handle Form Data
     *
     * @return boolean
     *
     **/
    private function validateFormRequest()
    {
        $arrError = [];

        $arrPost = $this->getRequest();
        $objEscaper = $this->objEscaper;

        if (!$objEscaper->escapeHtml($arrPost->getParam('email')))
        {
            $arrError[] = 'Email is required';
        }

        if (!$objEscaper->escapeHtml($arrPost->getParam('firstName')))
        {
            $arrError[] = 'First Name is required';
        }

        if (strlen($objEscaper->escapeHtml($arrPost->getParam('firstName'))) > 45)
        {
            $arrError[] = 'First Name should be less than 45 characters';
        }

        if (!$objEscaper->escapeHtml($arrPost->getParam('lastName')))
        {
            $arrError[] = 'Last Name is required';
        }

        if (strlen($objEscaper->escapeHtml($arrPost->getParam('lastName'))) > 45)
        {
            $arrError[] = 'Last Name should be less than 45 characters';
        }

        if (!$objEscaper->escapeHtml($arrPost->getParam('address')))
        {
            $arrError[] = 'Street Address is required';
        }

        if (strlen($objEscaper->escapeHtml($arrPost->getParam('address'))) > 150)
        {
            $arrError[] = 'Street Address should be less than 150 characters';
        }

        if (!$objEscaper->escapeHtml($arrPost->getParam('city')))
        {
            $arrError[] = 'City is required';
        }

        if (strlen($objEscaper->escapeHtml($arrPost->getParam('city'))) > 100)
        {
            $arrError[] = 'City should be less than 100 characters';
        }

        if (!$objEscaper->escapeHtml($arrPost->getParam('postalCode')))
        {
            $arrError[] = 'Postal/Zip code is required';
        }

        if (strlen($objEscaper->escapeHtml($arrPost->getParam('postalCode'))) > 20)
        {
            $arrError[] = 'Postal/Zip code should be less than 20 characters';
        }

        if (!$objEscaper->escapeHtml($arrPost->getParam('country')))
        {
            $arrError[] = 'Country is required';
        }

        if (strlen($objEscaper->escapeHtml($arrPost->getParam('country'))) > 2)
        {
            $arrError[] = 'Country should be less than 2 characters';
        }

        if (!$objEscaper->escapeHtml($arrPost->getParam('phoneNumber')))
        {
            $arrError[] = 'Phone number is required';
        }

        if (strlen($objEscaper->escapeHtml($arrPost->getParam('phoneNumber'))) > 25)
        {
            $arrError[] = 'Phone number should be less than 25 characters';
        }

        if (!empty($arrError))
        {
            $this->addLogger($arrError);

            return false;
        }

        return true;
    }

    private function addLogger($arrMessage = [])
    {
        $this->objJson->setHttpResponseCode(\Magento\Framework\Webapi\Exception::HTTP_BAD_REQUEST);
        $this->objJson->setData($arrMessage);

        syslog(LOG_CRIT, __CLASS__ . ':' . __METHOD__ . ':' . __LINE__ . ':' . ' Attacker IP:' . $this->getAttackerIPAddr() . ' - accessing directly');

        if (!empty($arrMessage))
        {
            foreach($arrMessage as $message)
            {
                syslog(LOG_CRIT, __CLASS__ . ':' . __METHOD__ . ':' . __LINE__ . ':' . ' Attacker IP:' . $this->getAttackerIPAddr() . ' - ' . $message);
            }
        }
    }
}