<?php

namespace XhapeSolutions\MembershipOrder\Controller\MultipleOrder;

use PayMaya\PayMayaSDK;
use PayMaya\API\Checkout;
use PayMaya\Model\Checkout\Buyer;
use PayMaya\Model\Checkout\ItemAmountDetails;
use PayMaya\Model\Checkout\ItemAmount;
use PayMaya\Model\Checkout\Item;
use PayMaya\Model\Checkout\Contact;
use PayMaya\Model\Checkout\Address;
use PayMaya\API\Webhook;

class Create extends Payment
{
    public function execute()
    {
        try
        {
            if (!$this->validateFormRequest())
            {
                return $this->getJSON();
            }

            $strSku = $this->getEscaper()->escapeHtml($this->getRequest()->getParam('card'));

            $this->getProduct()->load($this->getProduct()->getIdBySku($strSku));

            if (!$this->getProduct()->getId())
            {
                throw new \Exception("Err: Membership promotion card not found");
            }

            $objOrder = $this->checkout();

            $strCheckoutUrl = $this->createOrder($objOrder);

            $this->getJSON()->setData($strCheckoutUrl);
        }
        catch(\Exception $objError)
        {
            $this->addLogger([
                $objError->getMessage()
            ]);
        }

        return $this->getJSON();
    }

    private function createOrder($objOrder)
    {
        try
        {
            if (!session_id())
            {
                session_start();
            }

            if (!empty($_POST))
            {
                $strResult = $this->generateCheckoutUrl($objOrder->getEntityId());

                if ($strResult)
                {
                    return $strResult;
                }
                else
                {
                    throw new \Exception("Err: Generating checkout");
                }
            }
            else
            {
                throw new \Exception("Err: No Data");
            }
        }
        catch (\Exception $objError)
        {
            $this->addLogger([$objError->getMessage()]);
        }

        return false;
    }

    /**
     * @readme: Generate PayMaya checkout
     *
     * @return $strUri
     *
     * */
    private function generateCheckoutUrl($intOrderRef)
    {
        $objEscaper = $this->getEscaper();
        $arrPost = $this->getRequest()->getParam("multiple");
        $objPost = $arrPost[0];

        PayMayaSDK::getInstance()->initCheckout(self::$PUBLIC_KEY, self::$API_KEY, self::$PAYMAYA_ENV);

        // Checkout
        $objCheckout = new Checkout();

        // Contact
        $objContact = new Contact();
        $objContact->phone = $objEscaper->escapeHtml($objPost["mobileNumber"]);
        $objContact->email = $objEscaper->escapeHtml($objPost["email"]);

        // Address
        $objAddress = new Address();
        $objAddress->line1 = $objEscaper->escapeHtml($objPost["address"]);
        $objAddress->line2 = "";
        $objAddress->city = $objEscaper->escapeHtml($objPost["city"]);
        $objAddress->state = "";
        $objAddress->zipCode = $objEscaper->escapeHtml($objPost["postalCode"]);
        $objAddress->countryCode = $objEscaper->escapeHtml($objPost["country"]);

        $objBuyer = new Buyer();
        $objBuyer->firstName = $objEscaper->escapeHtml($objPost["firstName"]);
        $objBuyer->middleName = null;
        $objBuyer->lastName = $objEscaper->escapeHtml($objPost["lastName"]);
        $objBuyer->contact = $objContact;
        $objBuyer->shippingAddress = $objAddress;
        $objBuyer->billingAddress = $objAddress;

        $objCheckout->buyer = $objBuyer;

        // Item
        $objItemAmountDetails = new ItemAmountDetails();
        $objItemAmountDetails->shippingFee = "0";
        $objItemAmountDetails->tax = "0";
        $objItemAmountDetails->subtotal = $this->getProduct()->getPrice();

        $objItemAmount = new ItemAmount();
        $objItemAmount->currency = "PHP";
        $objItemAmount->value = number_format($this->getProduct()->getPrice(), 2, ".", "");
        $objItemAmount->details = $objItemAmountDetails;

        $strItemDesc = $this->getProduct()->getDescription();
        $strItemDescription = trim($strItemDesc) ? substr(strip_tags($strItemDesc), 1,255) : "no description";

        $objItem = new Item();
        $objItem->name = $this->getProduct()->getName();
        $objItem->code = $this->getProduct()->getSku();
        $objItem->description = $strItemDescription;
        $objItem->quantity = "1";
        $objItem->amount = $objItemAmount;
        $objItem->totalAmount = $objItemAmount;

        $objCheckout->items = array($objItem);
        $objCheckout->totalAmount = $objItemAmount;
        $objCheckout->requestReferenceNumber = (string)$intOrderRef;
        $objCheckout->redirectUrl = array(
            "success" => $this->getStoreBaseUrl() . "membership-card-transaction?status=success&o=" . (str_pad($intOrderRef,10,"0",STR_PAD_LEFT)),
            "failure" => $this->getStoreBaseUrl() . "membership-card-transaction?status=fail",
            "cancel" => $this->getStoreBaseUrl() . "membership-card-transaction?status=cancel"
        );

        $objCheckout->execute();

        $this->registerIPN();

        return $objCheckout->url; // Checkout URL
    }

    private function checkout()
    {
        try
        {
            $arrPost = $this->getRequest()->getParam("multiple");
            $objEscaper = $this->getEscaper();

            $this->getCart()->truncate();
            $this->getCart()->addProduct($this->getProduct(), [
                'qty' => 1
            ]);

            $objQuote = $this->getCart()->getQuote();

            $objQuote->setStore($this->getStoreManager()->getStore("membership_store_view"));

            //Set Customer
            $objQuote->setCustomerEmail($objEscaper->escapeHtml($arrPost[0]["email"]));
            $objQuote->setCustomerIsGuest(true);

            // Set Sales Order Payment
            $objQuote->getPayment()->setMethod('checkmo');

            $arrGenderAndBirthday = [];

            $objPayer = $arrPost[0];

            $arrShippingAddress = [
                "region" => "",
                "region_id" => 0,
                "country_id" => $objEscaper->escapeHtml($objPayer["country"]),
                "street" => [
                    $objEscaper->escapeHtml($objPayer["address"])
                ],
                "company" => "",
                "telephone" => $objEscaper->escapeHtml($objPayer["phoneNumber"]),
                "postcode" => $objEscaper->escapeHtml($objPayer["postalCode"]),
                "city" => $objEscaper->escapeHtml($objPayer["city"]),
                "firstname" => $objEscaper->escapeHtml($objPayer["firstName"]),
                "lastname" => $objEscaper->escapeHtml($objPayer["lastName"]),
                "email" => $objEscaper->escapeHtml($objPayer["email"]),
                "prefix" => "",
                "region_code" => ""
            ];

            //Set Address to quote
            $objQuote->getBillingAddress()->addData($arrShippingAddress);

            $saveOrderMemberDetails = [];

            foreach ($arrPost as $key => $objPost)
            {
                $saveOrderMemberDetails[] = [
                    "region" => "",
                    "region_id" => 0,
                    "country_id" => $objEscaper->escapeHtml($objPost["country"]),
                    "street" => [
                        $objEscaper->escapeHtml($objPost["address"])
                    ],
                    "company" => "",
                    "telephone" => $objEscaper->escapeHtml($objPost["phoneNumber"]),
                    "postcode" => $objEscaper->escapeHtml($objPost["postalCode"]),
                    "city" => $objEscaper->escapeHtml($objPost["city"]),
                    "firstname" => $objEscaper->escapeHtml($objPost["firstName"]),
                    "lastname" => $objEscaper->escapeHtml($objPost["lastName"]),
                    "email" => $objEscaper->escapeHtml($objPost["email"]),
                    "prefix" => "",
                    "region_code" => ""
                ];

                $memberCount = $key + 1;
                $arrGenderAndBirthday[] = "Member {$memberCount}: Gender: {$objEscaper->escapeHtml($objPost['gender'])}, " .
                    "Birthday: {$objEscaper->escapeHtml($objPost['birthday'])}";
            }

            $objShippingAddress = $objQuote->getShippingAddress()->addData($arrShippingAddress);
            $objShippingRates = $objShippingAddress->collectShippingRates();
            $objShippingRates->setShippingMethod('freeshipping_freeshipping');

            $this->getCart()->save();

            // Create Order From Quote
            $objOrder = $this->getQuoteManagement()->submit($objQuote);

            $objOrder->setEmailSent(0);
            $objOrder->setCustomerNoteNotify(false);

            $objOrder->addStatusHistoryComment(
                "Partner Code: {$this->getEscaper()->escapeHtml($this->getRequest()->getParam('partnerCode'))}, " .
                implode(" | ", $arrGenderAndBirthday)
            );

            $objOrder->save();

            $fOrder = fopen("multiple_order_{$objOrder->getEntityId()}.json", "w");
            fwrite($fOrder, json_encode($saveOrderMemberDetails));
            fclose($fOrder);

            return $objOrder;
        }
        catch (\Exception $objError)
        {
            /**
             * @todo: for now manually override the fix in mage 2.2 https://github.com/magento/magento2/commit/1fb3a97b4f394ae05d7afc5a213d0eb22d6e7c2e
             *
             * app/code/Magento/Quote/Model/Quote.php
             *
             * public function getItemById($itemId)
             * {
             *      //return $this->getItemsCollection()->getItemById($itemId);
             *      foreach ($this->getItemsCollection() as $item)
             *      {
             *          if ($item->getId() == $itemId)
             *          {
             *              return $item;
             *          }
             *      }
             *      return false;
             * }
             * */
            throw new \Exception($objError->getMessage());
        }

        return false;
    }

    /**
     * @readme: Handle Form Data
     *
     * @return boolean
     *
     **/
    private function validateFormRequest()
    {
        $arrError = [];

        $arrPost = $this->getRequest()->getParam("multiple");
        $objEscaper = $this->getEscaper();

        if (is_array($arrPost))
        {
            foreach ($arrPost as $key => $objPost)
            {
                if (!$objEscaper->escapeHtml($objPost["email"]))
                {
                    $arrError[] = "Member {$key}: Email is required";
                }

                if (!$objEscaper->escapeHtml($objPost["firstName"]))
                {
                    $arrError[] = "Member {$key}: First Name is required";
                }

                if (strlen($objEscaper->escapeHtml($objPost["firstName"])) > 45)
                {
                    $arrError[] = "Member {$key}: First Name should be less than 45 characters";
                }

                if (!$objEscaper->escapeHtml($objPost["lastName"]))
                {
                    $arrError[] = "Member {$key}: Last Name is required";
                }

                if (strlen($objEscaper->escapeHtml($objPost["lastName"])) > 45)
                {
                    $arrError[] = "Member {$key}: Last Name should be less than 45 characters";
                }

                if (!$objEscaper->escapeHtml($objPost["address"]))
                {
                    $arrError[] = "Member {$key}: Street Address is required";
                }

                if (strlen($objEscaper->escapeHtml($objPost["address"])) > 150)
                {
                    $arrError[] = "Member {$key}: Street Address should be less than 150 characters";
                }

                if (!$objEscaper->escapeHtml($objPost["city"]))
                {
                    $arrError[] = "Member {$key}: City is required";
                }

                if (strlen($objEscaper->escapeHtml($objPost["city"])) > 100)
                {
                    $arrError[] = "Member {$key}: City should be less than 100 characters";
                }

                if (!$objEscaper->escapeHtml($objPost["postalCode"]))
                {
                    $arrError[] = "Member {$key}: Postal/Zip code is required";
                }

                if (strlen($objEscaper->escapeHtml($objPost["postalCode"])) > 20)
                {
                    $arrError[] = "Member {$key}: Postal/Zip code should be less than 20 characters";
                }

                if (!$objEscaper->escapeHtml($objPost["country"]))
                {
                    $arrError[] = "Member {$key}: Country is required";
                }

                if (strlen($objEscaper->escapeHtml($objPost["country"])) > 2)
                {
                    $arrError[] = "Member {$key}: Country should be less than 2 characters";
                }

                if (!$objEscaper->escapeHtml($objPost["phoneNumber"]))
                {
                    $arrError[] = "Member {$key}: Phone number is required";
                }

                if (strlen($objEscaper->escapeHtml($objPost["phoneNumber"])) > 25)
                {
                    $arrError[] = "Member {$key}: Phone number should be less than 25 characters";
                }
            }
        }

        if (!empty($arrError))
        {
            $this->addLogger($arrError);

            return false;
        }

        return true;
    }

    private function registerIPN()
    {
        if (self::$PAYMAYA_ENV == "SANDBOX")
        {
            $arrHooks = Webhook::retrieve();

            foreach($arrHooks as $objHook)
            {
                $objHook->delete();
            }
        }

        $objWebhook = new Webhook();
        $objWebhook->name = Webhook::CHECKOUT_SUCCESS;
        $objWebhook->callbackUrl = $this->getStoreBaseUrl() . "paymaya/process/paymentsuccess";
        $objWebhook->register();

        // cancelled
        $objWebhook = new Webhook();
        $objWebhook->name = "CHECKOUT_DROPOUT";
        $objWebhook->callbackUrl = $this->getStoreBaseUrl() . "paymaya/process/paymentcancel";
        $objWebhook->register();

        $objWebhook = new Webhook();
        $objWebhook->name = Webhook::CHECKOUT_FAILURE;
        $objWebhook->callbackUrl = $this->getStoreBaseUrl() . "paymaya/process/paymentfail";
        $objWebhook->register();
    }
}