<?php
/**
 * @author     XhapeSolutions
 * @copyright  2017 XhapeSolutions
 */
\Magento\Framework\Component\ComponentRegistrar::register(
    \Magento\Framework\Component\ComponentRegistrar::MODULE,
    'XhapeSolutions_MembershipOrder',
    __DIR__
);