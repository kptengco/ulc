<?php

namespace XhapeSolutions\Paymaya\Controller\Process;

class Paymentsuccess extends Payment
{
    private $objCustomerFactory;
    private $objAddressFactory;
    private $objDistributornewaccountemail;
    private $bolEmailSent = false;

    private $isMultipleCard = false;
    private $arrMultipleMembership = [];

    public function __construct(\Magento\Framework\App\Action\Context $objContext,
                                \Magento\Framework\Controller\Result\JsonFactory $objJsonFactory,
                                \Magento\Catalog\Model\Product $objProduct,
                                \Magento\Checkout\Model\Cart $objCart,
                                \Magento\Quote\Model\QuoteManagement $objQuoteManagement,
                                \Magento\Framework\Escaper $objEscaper,
                                \Magento\Store\Model\StoreManagerInterface $objStoreManager,
                                \Magento\Framework\DB\TransactionFactory $objTransactionFactory,
                                \Magento\Sales\Model\Order $objOrder,
                                \Magento\Customer\Model\CustomerFactory $objCustomerFactory,
                                \Magento\Customer\Model\AddressFactory $objAddressFactory,
                                Distributornewaccountemail $objDistributornewaccountemail
    )
    {
        parent::__construct(
            $objContext,
            $objJsonFactory,
            $objProduct,
            $objCart,
            $objQuoteManagement,
            $objEscaper,
            $objStoreManager,
            $objTransactionFactory,
            $objOrder
        );

        $this->objCustomerFactory = $objCustomerFactory;
        $this->objAddressFactory = $objAddressFactory;
        $this->objDistributornewaccountemail = $objDistributornewaccountemail;
    }

    public function execute()
    {
        $arrResponse = $this->getPayMayaResponse();

        if (!empty($arrResponse))
        {
            if (isset($arrResponse["requestReferenceNumber"]))
            {
                $this->getOrder()->load((int)$arrResponse["requestReferenceNumber"]);

                try
                {
                    if(!$this->getOrder()->canInvoice())
                    {
                        $this->getOrder()->setStatus(\Magento\Sales\Model\Order::STATE_PAYMENT_REVIEW);
                        $this->getOrder()->addStatusHistoryComment("Order cannot be invoice.", false);
                    }
                    else
                    {
                        $objInvoice = $this->getOrder()->prepareInvoice();
                        $objInvoice->setRequestedCaptureCase(\Magento\Sales\Model\Order\Invoice::CAPTURE_OFFLINE);
                        $objInvoice->register();
                        $objInvoice->getOrder()->setStatus(\Magento\Sales\Model\Order::STATE_COMPLETE);

                        $objTransaction = $this->getTransactionFactory()->create()
                            ->addObject($objInvoice)
                            ->addObject($objInvoice->getOrder());

                        $objTransaction->save();

                        $strFile = "product_order_{$this->getOrder()->getEntityId()}.txt";

                        if (@file_exists($strFile))
                        {
                            unlink($strFile);
                        }
                        else
                        {
                            // create customer
                            $this->createCustomer();
                        }
                    }
                }
                catch (\Exception $objError)
                {
                    $this->getOrder()->setStatus(\Magento\Sales\Model\Order::STATE_PAYMENT_REVIEW);
                    $this->getOrder()->addStatusHistoryComment("Exception Message: {$objError->getMessage()}", false);
                }

                $this->getOrder()->addStatusHistoryComment(json_encode($arrResponse)); //save paymaya response

                if ($this->isMultipleCard)
                {
                    foreach ($this->arrMultipleMembership as $objMemberDetails)
                    {
                        $this->getOrder()->addStatusHistoryComment($objMemberDetails);
                    }
                }

                $this->getOrder()->save();
            }
        }
    }

    private function createCustomer()
    {
        $objStore = $this->getStoreManager()->getStore();
        $intWebsiteId = $objStore->getWebsiteId();

        $strFile = "multiple_order_{$this->getOrder()->getEntityId()}.json";

        if (@file_exists($strFile))
        {
            $this->isMultipleCard = true;

            $arrData = json_decode(file_get_contents($strFile), true);

            foreach ($arrData as $index => $objAddress)
            {
                $objCustomer = $this->objCustomerFactory->create();

                $intCounter = $index + 1;
                $this->arrMultipleMembership[] = "Member #{$intCounter}: Last Name: {$objAddress['lastname']}"
                . ", First Name: {$objAddress['firstname']}"
                . ", Email: {$objAddress['email']}";

                $objCustomer->setWebsiteId($intWebsiteId)
                    ->setStore($objStore)
                    ->setFirstname($objAddress['firstname'])
                    ->setMembershipCardNumber("-")
                    ->setMembershipCardExpiry("-")
                    ->setLastname($objAddress['lastname'])
                    ->setEmail($objAddress['email'])
                    ->setPassword(str_shuffle("uL1#zxCvL9854"));

                $objCustomer->save();

                $objAddressFactory = $this->objAddressFactory->create();

                $objAddressFactory->setCustomer($objCustomer)
                    ->setFirstname($objAddress['firstname'])
                    ->setLastname($objAddress['lastname'])
                    ->setCountryId($objAddress['country_id'])
                    ->setPostcode($objAddress['postcode'])
                    ->setCity($objAddress['city'])
                    ->setTelephone($objAddress['telephone'])
                    ->setFax("")
                    ->setCompany($objAddress['company'])
                    ->setStreet($objAddress['street'])
                    ->setIsDefaultBilling('1')
                    ->setIsDefaultShipping('1')
                    ->setSaveInAddressBook('1');

                $objAddressFactory->save();
            }

            unlink($strFile);
        }
        else
        {
            $strDistributorFile = "distributor_order_{$this->getOrder()->getEntityId()}.txt";
            $bolDistributor = false;

            if (@file_exists($strDistributorFile))
            {
                $bolDistributor = true;
                unlink($strDistributorFile);
            }

            $objCustomer = $this->objCustomerFactory->create();

            foreach ($this->getOrder()->getAddresses() as $objAddress) {
                if ($objAddress->getAddressType() == 'shipping' && !$objAddress->isDeleted()) {
                    $objCustomer->setWebsiteId($intWebsiteId)
                        ->setStore($objStore)
                        ->setFirstname($objAddress['firstname'])
                        ->setMembershipCardNumber("-")
                        ->setMembershipCardExpiry("-")
                        ->setAccountDistributor($bolDistributor ? "Y" : "N")
                        ->setLastname($objAddress['lastname'])
                        ->setEmail($objAddress['email'])
                        ->setPassword(str_shuffle("uL1#zxCvL9854"));

                    $objCustomer->save();

                    if ($bolDistributor && !$this->bolEmailSent)
                    {
                        $this->bolEmailSent = true;
                        $this->objDistributornewaccountemail->setCustomer($objCustomer);
                        $this->objDistributornewaccountemail->execute();
                    }

                    $objAddressFactory = $this->objAddressFactory->create();

                    $objAddressFactory->setCustomer($objCustomer)
                        ->setFirstname($objAddress['firstname'])
                        ->setLastname($objAddress['lastname'])
                        ->setCountryId($objAddress['country_id'])
                        ->setPostcode($objAddress['postcode'])
                        ->setCity($objAddress['city'])
                        ->setTelephone($objAddress['telephone'])
                        ->setFax("")
                        ->setCompany($objAddress['company'])
                        ->setStreet($objAddress['street'])
                        ->setIsDefaultBilling('1')
                        ->setIsDefaultShipping('1')
                        ->setSaveInAddressBook('1');

                    $objAddressFactory->save();
                }
            }
        }
    }
}