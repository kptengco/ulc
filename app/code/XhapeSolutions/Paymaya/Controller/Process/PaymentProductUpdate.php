<?php

namespace XhapeSolutions\Paymaya\Controller\Process;

abstract class PaymentProductUpdate extends Payment
{
    private $objStockRegistryInterface;

    public function __construct(\Magento\Framework\App\Action\Context $objContext,
                                \Magento\Framework\Controller\Result\JsonFactory $objJsonFactory,
                                \Magento\Catalog\Model\Product $objProduct,
                                \Magento\Checkout\Model\Cart $objCart,
                                \Magento\Quote\Model\QuoteManagement $objQuoteManagement,
                                \Magento\Framework\Escaper $objEscaper,
                                \Magento\Store\Model\StoreManagerInterface $objStoreManager,
                                \Magento\Framework\DB\TransactionFactory $objTransactionFactory,
                                \Magento\Sales\Model\Order $objOrder,
                                \Magento\CatalogInventory\Api\StockRegistryInterface $objStockRegistryInterface
    )
    {
        parent::__construct(
            $objContext,
            $objJsonFactory,
            $objProduct,
            $objCart,
            $objQuoteManagement,
            $objEscaper,
            $objStoreManager,
            $objTransactionFactory,
            $objOrder
        );

        $this->objStockRegistryInterface = $objStockRegistryInterface;
    }

    protected function addProductQty()
    {
        foreach ($this->getOrder()->getAllItems() as $objProduct)
        {
            $this->getProduct()->load($this->getProduct()->getIdBySku($objProduct->getSku()));

            $objStock = $this->objStockRegistryInterface->getStockItem($this->getProduct()->getId());

            $intNewQty = $objStock->getQty() + 1;

            $this->getProduct()->setStockData(["qty" => $intNewQty]);
            $this->getProduct()->setQuantityAndStockStatus(["qty" => $intNewQty, "is_in_stock" => true]);
            $this->getProduct()->save();
        }
    }
}