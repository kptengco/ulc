<?php

namespace XhapeSolutions\Paymaya\Controller\Process;

class Paymentfail extends PaymentProductUpdate
{
    public function execute()
    {
        try
        {
            $arrResponse = $this->getPayMayaResponse();

            if (!empty($arrResponse))
            {
                if (isset($arrResponse["requestReferenceNumber"]))
                {
                    $this->getOrder()->load((int)$arrResponse["requestReferenceNumber"]);

                    $this->addProductQty();

                    $this->getOrder()->setStatus(\Magento\Sales\Model\Order::STATE_PAYMENT_REVIEW);

                    $strError = "Order failed.";

                    $this->getOrder()->addStatusHistoryComment($strError);
                    $this->getOrder()->addStatusHistoryComment(json_encode($arrResponse)); //save paymaya response

                    $this->getOrder()->save();
                }
            }
        }
        catch (\Exception $objError)
        {

        }
    }
}