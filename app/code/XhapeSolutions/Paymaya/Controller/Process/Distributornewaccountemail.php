<?php

namespace XhapeSolutions\Paymaya\Controller\Process;

class Distributornewaccountemail extends \Magento\Framework\App\Action\Action
{

    /**
     * Recipient email config path
     */
    const XML_PATH_EMAIL_RECIPIENT = "distributor/email";

    /**
     * Recipient name config path
     */
    const XML_PATH_NAME_RECIPIENT = "distributor/name";

    /**
     * Recipient email template
     */
    const XML_EMAIL_TEMPLATE = "distributor_new_account_template";

    /**
     * @var \Magento\Framework\Mail\Template\TransportBuilder
     */
    protected $objTransportBuilder;

    /**
     * @var \Magento\Framework\Translate\Inline\StateInterface
     */
    protected $objInlineTranslation;

    /**
     * @var \Magento\Framework\App\Config\ScopeConfigInterface
     */
    protected $objScopeConfig;

    /**
     * @var \Magento\Store\Model\StoreManagerInterface
     */
    protected $objStoreManager;

    private $objCustomer = [];

    /**
     * @param \Magento\Framework\App\Action\Context $objContext
     * @param \Magento\Framework\Mail\Template\TransportBuilder $objTransportBuilder
     * @param \Magento\Framework\Translate\Inline\StateInterface $objInlineTranslation
     * @param \Magento\Framework\App\Config\ScopeConfigInterface $objScopeConfig
     * @param \Magento\Store\Model\StoreManagerInterface $objStoreManager
     * @param \Magento\Store\Model\StoreManagerInterface $objEscaper
     */
    public function __construct(
        \Magento\Framework\App\Action\Context $objContext,
        \Magento\Framework\Mail\Template\TransportBuilder $objTransportBuilder,
        \Magento\Framework\Translate\Inline\StateInterface $objInlineTranslation,
        \Magento\Framework\App\Config\ScopeConfigInterface $objScopeConfig,
        \Magento\Store\Model\StoreManagerInterface $objStoreManager
    )
    {
        parent::__construct($objContext);
        $this->objTransportBuilder = $objTransportBuilder;
        $this->objInlineTranslation = $objInlineTranslation;
        $this->objScopeConfig = $objScopeConfig;
        $this->objStoreManager = $objStoreManager;
    }

    public function setCustomer(\Magento\Customer\Model\Customer $objCustomer = null)
    {
        $this->objCustomer = $objCustomer;
    }

    /**
     * Send email for new distributor account
     *
     * @return void
     * @throws \Exception
     */
    public function execute()
    {
        if (!$this->objCustomer)
        {
            return;
        }

        $this->objInlineTranslation->suspend();

        try
        {
            $objPostObject = new \Magento\Framework\DataObject();
            $objPostObject->setData($this->objCustomer->getData());

            $objStoreScope = \Magento\Store\Model\ScopeInterface::SCOPE_STORE;

            $objSender = [
                "name" => $this->objScopeConfig->getValue(self::XML_PATH_NAME_RECIPIENT, $objStoreScope),
                "email" => $this->objScopeConfig->getValue(self::XML_PATH_EMAIL_RECIPIENT, $objStoreScope),
            ];

            $objTransport = $this->objTransportBuilder
                ->setTemplateIdentifier(self::XML_EMAIL_TEMPLATE)
                ->setTemplateOptions(
                    [
                        "area" => \Magento\Framework\App\Area::AREA_FRONTEND, // this is using frontend area to get the template file
                        "store" => \Magento\Store\Model\Store::DEFAULT_STORE_ID,
                    ]
                )
                ->setTemplateVars(["data" => $objPostObject])
                ->setFrom($objSender)
                ->addTo(
                    $this->objCustomer->getEmail(),
                    $this->objCustomer->getFirstname() . " " . $this->objCustomer->getLastname()
                )
                ->getTransport();

            $objTransport->sendMessage();
        }
        catch (\Exception $objError)
        {
            echo $objError->getMessage();
        }
    }
}