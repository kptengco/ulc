<?php

namespace XhapeSolutions\Paymaya\Controller\Process;

class Payment extends \Magento\Framework\App\Action\Action
{
    protected static $PUBLIC_KEY;
    protected static $API_KEY;
    protected static $PAYMAYA_ENV;

    private $objJsonFactory;
    private $objJson;
    private $objProduct;
    private $objCart;
    private $objQuoteManagement;
    private $objEscaper;
    private $objStoreManager;
    private $objOrder;
    private $objTransactionFactory;

    public function __construct(\Magento\Framework\App\Action\Context $objContext,
                                \Magento\Framework\Controller\Result\JsonFactory $objJsonFactory,
                                \Magento\Catalog\Model\Product $objProduct,
                                \Magento\Checkout\Model\Cart $objCart,
                                \Magento\Quote\Model\QuoteManagement $objQuoteManagement,
                                \Magento\Framework\Escaper $objEscaper,
                                \Magento\Store\Model\StoreManagerInterface $objStoreManager,
                                \Magento\Framework\DB\TransactionFactory $objTransactionFactory,
                                \Magento\Sales\Model\Order $objOrder
    )
    {
        parent::__construct($objContext);

        self::$PUBLIC_KEY = getenv("PAYMAYA_PUBLIC_KEY");
        self::$API_KEY = getenv("PAYMAYA_API_KEY");
        self::$PAYMAYA_ENV = getenv("PAYMAYA_ENV");

        $this->objJsonFactory = $objJsonFactory;
        $this->objProduct = $objProduct;
        $this->objCart = $objCart;
        $this->objQuoteManagement = $objQuoteManagement;
        $this->objEscaper = $objEscaper;
        $this->objStoreManager = $objStoreManager;
        $this->objOrder = $objOrder;
        $this->objTransactionFactory = $objTransactionFactory;

        $this->objJson = $this->objJsonFactory->create();
    }

    public function execute()
    {
        // TODO: Implement execute() method.
    }

    protected function getStoreManager()
    {
        return $this->objStoreManager;
    }

    protected function getJSON()
    {
        return $this->objJson;
    }

    protected function getEscaper()
    {
        return $this->objEscaper;
    }

    protected function getProduct()
    {
        return $this->objProduct;
    }

    protected function getOrder()
    {
        return $this->objOrder;
    }

    protected function getTransactionFactory()
    {
        return $this->objTransactionFactory;
    }

    protected function getCart()
    {
        return $this->objCart;
    }

    protected function getQuoteManagement()
    {
        return $this->objQuoteManagement;
    }

    protected function getAttackerIPAddr()
    {
        if (isset($_SERVER['HTTP_CLIENT_IP']) && $_SERVER['HTTP_CLIENT_IP'] != null)
        {
            return $_SERVER['HTTP_CLIENT_IP'];
        }
        else if (isset($_SERVER['HTTP_X_FORWARDED_FOR']) && $_SERVER['HTTP_X_FORWARDED_FOR'] != null)
        {
            return $_SERVER['HTTP_X_FORWARDED_FOR'];
        }
        else
        {
            return $_SERVER['REMOTE_ADDR'];
        }
    }

    protected function addLogger($arrMessage = [])
    {
        $this->objJson->setHttpResponseCode(\Magento\Framework\Webapi\Exception::HTTP_BAD_REQUEST);
        $this->objJson->setData($arrMessage);

        syslog(LOG_CRIT, __CLASS__ . ':' . __METHOD__ . ':' . __LINE__ . ':' . ' Attacker IP:' . $this->getAttackerIPAddr() . ' - accessing directly');

        if (!empty($arrMessage))
        {
            foreach($arrMessage as $message)
            {
                syslog(LOG_CRIT, __CLASS__ . ':' . __METHOD__ . ':' . __LINE__ . ':' . ' Attacker IP:' . $this->getAttackerIPAddr() . ' - ' . $message);
            }
        }
    }

    protected function getStoreBaseUrl()
    {
        $strUrl = str_replace("default/", "", $this->getStoreManager()->getStore()->getBaseUrl());

        return substr($strUrl, -1) == "/" ? $strUrl : $strUrl . "/";
    }

    protected function getPayMayaResponse()
    {
        $strJSON = file_get_contents("php://input");

        return json_decode($strJSON, TRUE);
    }
}