<?php

namespace XhapeSolutions\Member\Controller\Certificate;

class Seecode extends \XhapeSolutions\Member\Controller\Auth\Auth
{
    public function execute()
    {
        try
        {
            if (isset($_SESSION["ulc_customer"]))
            {
                $strSku = $this->getEscaper()->escapeHtml($this->getRequest()->getParam('sku'));
                $strCode = crypt($strSku, \XhapeSolutions\Merchant\Controller\User\Products::CERT_SALT);

                $this->getJSON()->setData($strCode);
            }
            else
            {
                $this->getJSON()->setHttpResponseCode(\Magento\Framework\Webapi\Exception::HTTP_UNAUTHORIZED);
                $this->getJSON()->setData(["You need to login first to be able to continue."]);
            }
        }
        catch(\Exception $objError)
        {
            $this->getJSON()->setHttpResponseCode(\Magento\Framework\Webapi\Exception::HTTP_INTERNAL_ERROR);
            $this->getJSON()->setData(["Failed to see certificate code. Please try again."]);
        }

        return $this->getJSON();
    }
}