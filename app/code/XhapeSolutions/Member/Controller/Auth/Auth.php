<?php

namespace XhapeSolutions\Member\Controller\Auth;

abstract class Auth extends \Magento\Framework\App\Action\Action
{
    private $objJsonFactory;
    private $objJson;
    private $objEscaper;
    private $objCustomer;
    private $objCustomerCollection;
    private $objStoreManager;
    private $objSession;
    private $objAccountManagement;

    public function __construct(
        \Magento\Framework\App\Action\Context $objContext,
        \Magento\Framework\Controller\Result\JsonFactory $objJsonFactory,
        \Magento\Store\Model\StoreManagerInterface $objStoreManager,
        \Magento\Framework\Escaper $objEscaper,
        \Magento\Customer\Model\Customer $objCustomer,
        \Magento\Customer\Model\ResourceModel\Customer\Collection $objCustomerCollection,
        \Magento\Customer\Model\Session $objSession,
        \Magento\Customer\Model\AccountManagement $objAccountManagement
    )
    {
        parent::__construct($objContext);

        $this->objJsonFactory = $objJsonFactory;
        $this->objEscaper = $objEscaper;
        $this->objJson = $this->objJsonFactory->create();
        $this->objCustomer = $objCustomer;
        $this->objCustomerCollection = $objCustomerCollection;
        $this->objStoreManager = $objStoreManager;
        $this->objSession = $objSession;
        $this->objAccountManagement = $objAccountManagement;

        if (!session_id())
        {
            session_start();
        }
    }

    protected function getJSON()
    {
        return $this->objJson;
    }

    protected function getEscaper()
    {
        return $this->objEscaper;
    }

    protected function getCustomer()
    {
        return $this->objCustomer;
    }

    protected function getCustomerCollection()
    {
        return $this->objCustomerCollection;
    }

    protected function getStoreManager()
    {
        return $this->objStoreManager;
    }

    protected function getSession()
    {
        return $this->objSession;
    }

    protected function getAccountManagement()
    {
        return $this->objAccountManagement;
    }
}