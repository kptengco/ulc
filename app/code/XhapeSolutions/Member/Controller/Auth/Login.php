<?php

namespace XhapeSolutions\Member\Controller\Auth;

class Login extends Auth
{
    public function execute()
    {
        try
        {
            if (isset($_SESSION["ulc_customer"]))
            {
                return $this->getJSON();
            }

            $strUsername = $this->getEscaper()->escapeHtml($this->getRequest()->getParam('membership_card'));


            if (is_numeric($strUsername))
            {
                $this->loginUsingMembershipCard($strUsername);
            }
            else
            {
                $this->loginUsingEmail($strUsername);
            }
        }
        catch (\Exception $objError)
        {
            $this->getJSON()->setHttpResponseCode(\Magento\Framework\Webapi\Exception::HTTP_INTERNAL_ERROR);
            $this->getJSON()->setData([$objError->getMessage()]);
        }

        return $this->getJSON();
    }

    private function loginUsingMembershipCard($strUsername)
    {
        $objCustomerCollection = $this->getCustomerCollection()
            ->addAttributeToSelect('*')
            ->addAttributeToFilter('membership_card_number', $strUsername)
            ->addAttributeToFilter('membership_card_expiry', ['notnull' => true])
            ->load();

        $arrCustomer = $objCustomerCollection->getData();

        if (!isset($arrCustomer[0]))
        {
            // not found
            $this->getJSON()->setHttpResponseCode(\Magento\Framework\Webapi\Exception::HTTP_BAD_REQUEST);
            $this->getJSON()->setData(["Invalid Membership Card", "Error: IC1"]);
        }
        else if (isset($arrCustomer[0]))
        {
            $strYearExpiry = date('Y', strtotime($arrCustomer[0]['membership_card_expiry']));
            $strMonthExpiry = date('m', strtotime($arrCustomer[0]['membership_card_expiry']));
            $strDateExpiry = date('d', strtotime($arrCustomer[0]['membership_card_expiry']));

            if ($strYearExpiry <= date('Y') &&
                $strMonthExpiry <= date('m') &&
                $strDateExpiry <= date('d')
            )
            {
                // expired
                $this->getJSON()->setHttpResponseCode(\Magento\Framework\Webapi\Exception::HTTP_BAD_REQUEST);
                $this->getJSON()->setData(["Invalid Membership Card", "Error: IC3"]);
            }
            else
            {
                $intWebsiteId = $this->getStoreManager()->getStore()->getWebsiteId();
                $strPassword = $this->getEscaper()->escapeHtml($this->getRequest()->getParam('password'));

                if ($this->getCustomer()->setWebsiteId($intWebsiteId)->authenticate($arrCustomer[0]["email"], $strPassword))
                {
                    $_SESSION["ulc_customer"] = [
                        "email" => $arrCustomer[0]["email"]
                    ];

                    $this->getSession()->loginById($arrCustomer[0]["entity_id"]);
                }
            }
        }
    }

    private function loginUsingEmail($strUsername)
    {
        $intWebsiteId = $this->getStoreManager()->getStore()->getWebsiteId();
        $strPassword = $this->getEscaper()->escapeHtml($this->getRequest()->getParam('password'));

        if ($this->getCustomer()->setWebsiteId($intWebsiteId)->authenticate($strUsername, $strPassword))
        {
            $_SESSION["ulc_customer"] = [
                "email" => $strUsername
            ];

            $this->getSession()->loginById($this->getCustomer()->getEntityId());
        }
        else
        {
            $this->getJSON()->setHttpResponseCode(\Magento\Framework\Webapi\Exception::HTTP_BAD_REQUEST);
            $this->getJSON()->setData(["Combination of username and password did not match."]);
        }
    }
}