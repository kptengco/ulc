<?php

namespace XhapeSolutions\Member\Controller\Auth;

class Changepassword extends Auth
{
    public function execute()
    {
        try
        {
            if ($this->getSession()->isLoggedIn())
            {
                $strCurrentPassword = (string)$this->getRequest()->getPost('current_password');
                $strPassword = (string)$this->getRequest()->getPost('password');
                $strConfirmPassword = (string)$this->getRequest()->getPost('confirm_password');

                if ($strConfirmPassword !== $strPassword)
                {
                    $this->getJSON()->setHttpResponseCode(\Magento\Framework\Webapi\Exception::HTTP_BAD_REQUEST);
                    $this->getJSON()->setData(["Password and confirm password didn't match"]);
                    return $this->getJSON();
                }
                else
                {
                    $this->getAccountManagement()->changePassword($_SESSION["ulc_customer"]["email"], $strCurrentPassword, $strPassword);
                }
            }
        }
        catch (\Magento\Framework\Exception\InputException $objInputException)
        {
            $this->getJSON()->setHttpResponseCode(\Magento\Framework\Webapi\Exception::HTTP_BAD_REQUEST);
            $this->getJSON()->setData([$objInputException->getMessage()]);
        }
        catch(\Exception $objError)
        {
            $this->getJSON()->setHttpResponseCode(\Magento\Framework\Webapi\Exception::HTTP_INTERNAL_ERROR);
            $this->getJSON()->setData([$objError->getMessage()]);
        }

        return $this->getJSON();
    }
}