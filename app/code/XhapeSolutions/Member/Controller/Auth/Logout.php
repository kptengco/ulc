<?php

namespace XhapeSolutions\Member\Controller\Auth;

class Logout extends Auth
{
    public function execute()
    {
        try
        {
            if (isset($_SESSION["ulc_customer"]))
            {
                unset($_SESSION["ulc_customer"]);

                $this->getSession()->logout();
            }

            // merchant
            if (isset($_SESSION["ulc_admin_sess_user_context_id"]))
            {
                unset($_SESSION['ulc_admin_sess_user_context_id']);
            }
        }
        catch(\Exception $objError)
        {
            $this->getJSON()->setHttpResponseCode(\Magento\Framework\Webapi\Exception::HTTP_INTERNAL_ERROR);
            $this->getJSON()->setData(["Failed to logout customer. Please try again."]);
        }

        return $this->getJSON();
    }
}