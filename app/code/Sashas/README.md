<h2>Magento 2 Customer Attribute Extension Example</h2>
This extension shows how to create customer attribute with Magento 2 extension.

*      The Full  Article  <a href="http://www.extensions.sashas.org/blog/magento-2-how-to-make-customer-attribute.html">here</a>

Lastly run:

php bin/magento setup:upgrade
php bin/magento cache:flush (if you had cache enabled)
php bin/magento indexer:reindex

**Note:
if upgrade did not work, remove the module (Sashas_CustomerAttribute) in setup_module table